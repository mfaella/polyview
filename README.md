# PolyView #
## a polyhedra visualizer ##

* Draws 2D sections and projections of polyhedra with an arbitrary number of dimensions.
* Version 0.1

## Dependencies ##

PolyView depends on the jogl and PPL libraries.
In order to properly build or run PolyView, the appropriate
paths should be set in a file called "paths.sh", in the same folder
as "run.sh".

A template called "paths-template.sh" is included in the distribution.

### jogl (Java bindings to OpenGL) ###

* Version 1.1.1 is known to work
* Make sure that the following files are present: jogl-all.jar, gluegen-rt.jar, libgluegen-rt.so

### Parma Polyhedra Library (PPL) with Java support ###

* Version 1.2 is known to work
* configure PPL with --with-java=<path-to-JDK>, then make
* Make sure that the following files are generated: ppl_java.jar and libppl_java.{so,jnilib}

## Using ##

Run "./run.sh".
Open a polyhedron file.
Enjoy.

## Input format ##

Check out the "examples" folder.

## Credits ##

* Marco Faella, marfaella@gmail.com
* Originally developed by Aniello Sorrentino for his Bachelor thesis in Computer Science