
package polyview.view;

import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JSlider;

/**
 *
 * @author nello
 */
public class ComponentGraphicVariable {
    private JLabel labelVariable;
    private List<JCheckBox> checkDraws;
    private JCheckBox checkProject;
    private JSlider   variatorVelocity;
    private JFormattedTextField textVariable;
    private boolean stato;

    public ComponentGraphicVariable(JLabel labelVariable, JCheckBox checkDraw, JCheckBox checkProject, JSlider variatorVelocity, JFormattedTextField textVariable) {
        //Inizializzo la lista dei checkDraws
        this.checkDraws=new ArrayList<>();
        if(labelVariable==null || checkDraw==null || checkProject==null||variatorVelocity==null||textVariable==null)
            throw new RuntimeException("Valori ComponentGraphicVariable nulli");
        
        //Inserisco il primo checkDraw
        this.checkDraws.add(checkDraw);
        //Gli altri componenti sono incomuni
        this.labelVariable = labelVariable;
        this.checkProject = checkProject;
        this.variatorVelocity = variatorVelocity;
        this.textVariable = textVariable;
        //Stato della varibile true
        stato=true;
    }
    
    public ComponentGraphicVariable(JLabel labelVariable, List<JCheckBox> checkDraws, JCheckBox checkProject, JSlider variatorVelocity, JFormattedTextField textVariable) {
        //Inizializzo la lista dei checkDraws
        this.checkDraws=new ArrayList<>();
        if(labelVariable==null || checkDraws==null || checkDraws.isEmpty()|| checkProject==null||variatorVelocity==null||textVariable==null)
            throw new RuntimeException("Valori COmponentGraphicVariable nulli");
        
        //Inserisco la lista di checkDraw
        this.checkDraws=checkDraws;
        //Gli altri componenti sono incomuni
        this.labelVariable = labelVariable;
        this.checkProject = checkProject;
        this.variatorVelocity = variatorVelocity;
        this.textVariable = textVariable;
        //Stato della varibile true
        stato=true;
    }

    public JLabel getLabelVariable() {
        return labelVariable;
    }
    public void setStato(boolean stato){
        this.stato=stato;
    }
    
    public boolean getStato(){
        return stato;
    }

    public JCheckBox getCheckDraw(int i) {
        JCheckBox ris=null;
        
        if(i>=0 && i<checkDraws.size())
            ris=this.checkDraws.get(i);
        
        return ris;
    }
    public List<JCheckBox> getCheckDraws(){
        return this.checkDraws;
    }
    public int getIndexCheckDraw(Object element){
        return checkDraws.indexOf(element);
    }
    public ItemListener getListener(){
        return checkDraws.get(0).getItemListeners()[0];
    }
    
    public JCheckBox removeCheckDraw(int i){
        if(i>checkDraws.size())
            throw new RuntimeException("Indice draw non corrispondente");
        
        return checkDraws.remove(i);
    }

    public void addCheckDraw(JCheckBox checkDraw) {
        this.checkDraws.add(checkDraw);
    }

    public JCheckBox getCheckProject() {
        return checkProject;
    }

    public JSlider getVariatorVelocity() {
        return variatorVelocity;
    }


    public JFormattedTextField getTextVariable() {
        return textVariable;
    }
}
