
package polyview.view;


import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import polyview.model.PPLModel;


/**
 *
 * @author nello
 */
public class MainFrame extends JFrame {
   
    private static final String PROGRAM_NAME = "PolyView";

    //Componenti della finestra principale
    private JTabbedPane pane;
    private JMenuBar barraApplication;
    private JFileChooser browsing;
    //Lista che contiene riferimenti ad tutti i pannelli creati dall'utente
    private List<PanelPolyhedrons> listPanelPoliedro;
    private String[] cmdLine;
    // We store this because this item needs to be enabled and disabled dynamically
    private JMenuItem open;
    
    /**
     * Costruttore per creare una nuova MainFrame
     */
    public MainFrame(String[] cmdLine) {
	this.cmdLine = cmdLine;

        //Inizializzo le componenti
        initComponents();
        //Imposto la chiusura del frame
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        
        //Imposto un ascoltatore alla chiusura del Frame che chiude tutti i frame creati dai vari pannelli
        this.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent e){
		for (PanelPolyhedrons pp: listPanelPoliedro)
		    pp.disposeAllFrames();
            }
        });
        //Imposto il nome della finestra
        setTitle(PROGRAM_NAME);
        //Imposto la dimensione minima
        this.setMinimumSize(new Dimension(800,600));
    }
    
    //Posiziona il frame al centro dello schermo 
    private void centerWindow(){
        //Centro il frame sullo schermo
        Dimension dim=getToolkit().getScreenSize();
        this.setLocation(dim.width/2 -this.getWidth()/2, dim.height/2 - this.getHeight()/2);
    }
    
    /*
     *Questo metodo è chiamato all'interno del costruttore per inizializzare il frame principale
     */
    private void initComponents(){
        
        //Creazione e gestione del Jpanned
        pane= new JTabbedPane();
        pane.setTabPlacement(JTabbedPane.TOP);
        //Inizializzo la lista
        listPanelPoliedro=new ArrayList<>();
        browsing=new JFileChooser();
        
        //Pattern strategy impostazione del layout del frame
        this.setLayout(new GridBagLayout());

        /*Disposizione del panned all'interno del frame*/
        GridBagConstraints constraintJTabbed = new GridBagConstraints();
        //Lo posizione alla prima riga e prima colonna(unico elemento del Frame)
        constraintJTabbed.gridx=0;
        constraintJTabbed.gridy=0;
        
        //Attributi per il ridimensionamento/ingrandimento(Imposto che si può prendere tutto lo spazio a disposizione sia in verticale che orizzontale)
        constraintJTabbed.weightx=1.0; 
        constraintJTabbed.weighty=1.0;
        //Si estende in entrambe le direzioni(Collegato agli attributi weightx ed weighty)
        constraintJTabbed.fill=GridBagConstraints.BOTH;
        //Lo posiziono in NorthWest(Angolo in alto a sinistra)
        constraintJTabbed.anchor=GridBagConstraints.NORTHWEST;
        //Imposto i margine superiore(top),magine inferiore(bottom),
        //margine destro(right),margine sinistro(left) direttamente istanzianto l'oggetto Insets
        constraintJTabbed.insets=new Insets(0,5,5,5);
        //Inserisco il JTabbed
        this.add(pane,constraintJTabbed);
        
        //Creazione e aggiunta del menuBar
        createMenuBar();
        setJMenuBar(barraApplication);
    }
    
    public void closeTab(int n) {
	pane.remove(n);
	//Dico al pannello di chiudere tutti i frame
	listPanelPoliedro.get(n).disposeAllFrames();
	//Rimuovo il pannello dalla lista
	listPanelPoliedro.remove(n);
	if (listPanelPoliedro.isEmpty())
	    open.setEnabled(false);
    }
    
    private void createMenuBar(){
        
       //Inizializzazione della barra menu
        barraApplication=new JMenuBar();
       
        // Elementi della barra
        JMenu file=new JMenu("File");
        
        // Sottelementi dell'elemento "File" della barra
        JMenuItem openInNewTab =new JMenuItem("Open in new tab"); //Apertura file
        open = new JMenuItem("Open"); //Apertura file
	open.setEnabled(false); // Initially disabled
        file.add(openInNewTab);
        file.add(open);
        
        openInNewTab.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent event){
                
                try {
                    openDocument(false);
                }
                catch(FileNotFoundException ex){
                     JOptionPane.showMessageDialog(MainFrame.this,"Error opening file","File Error",JOptionPane.ERROR_MESSAGE);
                }
                catch(IllegalArgumentException ex1){
                     JOptionPane.showMessageDialog(MainFrame.this,ex1.getMessage(),"Internal error",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        
        open.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent event){
                
                try {
                    openDocument(true);
                }
                catch(FileNotFoundException ex){
                     JOptionPane.showMessageDialog(MainFrame.this,"Error opening file", "File Error",JOptionPane.ERROR_MESSAGE);
                }
                catch(IllegalArgumentException ex1){
                     JOptionPane.showMessageDialog(MainFrame.this,ex1.getMessage(),"Internal error",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        
        
        JMenuItem itemExit=new JMenuItem("Exit");//Uscita dal programma
        file.add(itemExit);
        
        itemExit.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent event){
                //Far comparire un pannello di conferma
                System.exit(0);
            }
        });
        
        //Aggiunta degli elementi Menu alla barra
        barraApplication.add(file);
    }
    
    private void openDocument(boolean inTab) throws FileNotFoundException{
        PPLModel nncPoliedro;
        PanelPolyhedrons temp;
        int indexTab;
        if (browsing.showOpenDialog(MainFrame.this)!=JFileChooser.APPROVE_OPTION)
	    return;
            // JOptionPane.showMessageDialog(MainFrame.this, "No file selected", "File Error",JOptionPane.ERROR_MESSAGE);

        //Vedo se devo caricare i powerSet del file all'interno di un tab esistente oppure in uno nuovo
	if(!inTab){
	    nncPoliedro = PPLModel.inizializePoliedri(browsing.getSelectedFile());
	    if(nncPoliedro!=null && !nncPoliedro.isEmptyPolyhedrons()){
		String fileName = browsing.getSelectedFile().getName();
		//Inizializzo un nuovo pannello con i dati del file
		temp = new PanelPolyhedrons(fileName, nncPoliedro);
		//Aggiungo il pannello alla lista
		listPanelPoliedro.add(temp);
		
		int n = listPanelPoliedro.size() - 1;
		open.setEnabled(true); // the menu item
		
		//Setto ed aggiungo un nuovo tab corrispondente al pannello creato
		pane.add(fileName, temp);
		pane.setTabComponentAt(n, new ButtonTabComponent(this, fileName, n));
		pane.setSelectedIndex(n);
		// this.setSize(new Dimension(700,400));
	    }else
		JOptionPane.showMessageDialog(MainFrame.this,"Empty polyhedron","Errori vincoli",JOptionPane.ERROR_MESSAGE); //Vincoli producono un poliedro vuoto
	}
	else{
	    //Prima di effettuare il cast mi assicuro che sia un discendente di PanelPolyhedrons(vado comunque sicuro, se ho null vuol dire che non c'è ancora nessun tabed aperto)
	    if(pane.getSelectedComponent() instanceof PanelPolyhedrons == true){
		//Mi procuro l'indice di selezione 
		indexTab=pane.getSelectedIndex();
		//effettuo il cast per ottenere il riferimento di tipo PanelPolyhedrons
		temp=(PanelPolyhedrons)pane.getSelectedComponent();
		//Aggiorno il title del tabbed selezionato
		pane.setTitleAt(indexTab,pane.getTitleAt(indexTab)+","+browsing.getSelectedFile().getName());
		pane.updateUI();
		//Aggiungo i nuovi powerSet presenti nel file al modello
		PPLModel.inizializePoliedri(browsing.getSelectedFile(), temp.getModelloPolyhedrons());
		//Lo notifico al pannello
		temp.addPowerSet(browsing.getSelectedFile().getName());
	    }
	    else
		JOptionPane.showMessageDialog(MainFrame.this,"Internal error","Open Error",JOptionPane.ERROR_MESSAGE);
	}
	
    }
}
