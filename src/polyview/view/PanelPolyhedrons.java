 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polyview.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.border.SoftBevelBorder;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import polyview.model.Pair;
import polyview.model.PPLModel;
import polyview.model.PPLModel.ModelFrame;
import polyview.model.PairDraw;
import polyview.model.PowerSet;

/**
 * The panel for a tab, controlling all views.
 * @author nello
 */
public class PanelPolyhedrons extends JPanel{
    
    //Pannello Widget con il relativo JScrollPane
    private JPanel panelWidget;
    private JScrollPane ScrollPanelWidget;
    
    //Componenti per la ToolBar del pannello
    private JToolBar m_toolBar;
    private JToggleButton viewPatches;
    private JLabel labelShowPatches;
    
    //Componenti fissi del pannello 
    private List<JLabel> labelDraws;
    private JLabel labelProject;
    private JLabel labelFix;
    private JLabel labelPowerSets;
    private JLabel status;
    
    //Componenti per lo status del Canvas
    private JList listStatus;//View
    private DefaultListModel statusModel;//Model
     
    //Componente rappresentate il separatore tra la sezione draw ed l'altra(Project e fix)
    private JSeparator separatorWidget;
    
    //Lista con tutti i Frame che contengono i canvas creati (views)
    private List<FramePolyhedron> listFrame;

    //Button per aggiungere un nuovo canvas alla lista 
    private JButton addWindows;
    //Tabella che visualizza i PowerSet contenuti nel modello     
    JTable tablePowerSet;
     //Insieme delle variabili caricate nel pannello
    private Set<String> labelVariabili;
    //Stringhe di stato per lo status
    private final String stringError="Empty sets";
    private final String stringNoDraw="Inactive";
    private final String stringError1="Empty file";
    private final String stringCorrect="Drawing sets";
    
    //Stringa del nome del Tab
    private String nomeFile;
    
    //Lista di oggetti che rappresentano la singola variabile(ogni variabile è associata ad un set di componenti che la settano)
    private List<ComponentGraphicVariable> componentVariable;
    
    //Variabile che rappresenta il modello
    private PPLModel modelloPolyhedrons;
    
    public PanelPolyhedrons(String nomeFile,PPLModel nncPoliedro){
        //Check parametro 
        if(nncPoliedro==null || nomeFile==null)
             throw new NullPointerException(stringError1);
        
        //Associo il poliedro al pannello creato(cioè faccio il collegamento con il modello)
        this.modelloPolyhedrons=nncPoliedro;
        //Associo il nome alla stringa del pannello creato(Inserisco il nome tra parentesi)
        this.nomeFile="("+nomeFile+")";
        
        //Inizializzo i vari componenti
        initComponents();
        /*Inserimento e impostazione disposizione all'interno del frame principale*/
        //Pattern strategy impostazione del layout del panel
        setLayout(new GridBagLayout());
	setBorder(new SoftBevelBorder(SoftBevelBorder.RAISED ));        
        
	//Creazione ed disposizione della toolbar interna al Panel principale
        inizializeToolbar();
        
        //Creazione e disposizione del pannello all'interno del Panel principale
        inizializeWidgetPanel();
    }
    
    private void initComponents(){
        
        //Inizializzo il pannello principale con la relativa ScrollPane
        panelWidget=new JPanel();
        ScrollPanelWidget=new JScrollPane();
        
        //Inizializzazione variabili fisse del pannello variable
        labelDraws=new ArrayList<>();
        labelProject=new JLabel();
        labelFix=new JLabel();
        //Label per la tabella powerSet
        labelPowerSets=new JLabel();
        status=new JLabel();
        //Inizializzo gli item della toolbar
        labelShowPatches=new JLabel("Show patches");
        
        //Inizializzo il JSeparotor
        separatorWidget=new JSeparator(JSeparator.VERTICAL);
        
        //Button per l'aggiunta di un nuovo canvas alla lista
        addWindows=new JButton("Add view");
        
        //Inizializzo la lista dei frame associati al pannello
        listFrame = new ArrayList<>();
        //Inizializzo l'insieme che conterra le variabili
        labelVariabili=new HashSet<>();
        //Inizializzo gli insiemi delle varie componenti che rappresentano le variabili dinamiche
        componentVariable=new ArrayList<>();
    }

    
    private void inizializeToolbar(){
        //Creo la JtoolBar
        m_toolBar=new JToolBar("Commands");
        m_toolBar.setFloatable(false);
        
        //Opzione per visualizzare o meno le patches
        Icon image=new ImageIcon(getClass().getResource("images/view.png"));
        viewPatches=new JToggleButton(image);
        //Aggiungo i componenti alla toolbar
        m_toolBar.add(viewPatches);
        m_toolBar.add(labelShowPatches);
        
        viewPatches.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                if(viewPatches.isSelected())
                    for(FramePolyhedron temp:listFrame)
                        temp.setShowPatches(true);
                else
                    for(FramePolyhedron temp:listFrame)
                        temp.setShowPatches(false);
            }
        });
        
        //Disposizione della ToolBar
        GridBagConstraints constraintJToolBar = new GridBagConstraints();
        //Lo posizione nella prima riga e colonna
        constraintJToolBar.gridx=0;
        constraintJToolBar.gridy=0;
        //Occupa tutto lo spazio a disposizione orizzontalmente
        constraintJToolBar.weightx=1.0;
        //Si espande orizzontalmente
        constraintJToolBar.fill=GridBagConstraints.HORIZONTAL;
        //Lo posiziono in NorthWest(Angolo in alto a sinistra)
        constraintJToolBar.anchor=GridBagConstraints.NORTHWEST;
        //Aggiungo la toolBar al Pannello
        add(m_toolBar,constraintJToolBar);
    }
    //Impostazioni pannello
    private void inizializeWidgetPanel(){
        //Imposto il bordo del pannello con il titolo
        panelWidget.setBorder(BorderFactory.createTitledBorder("Widget"));
        //Inserisco il pannello principale in una JScrollPane
        ScrollPanelWidget.getViewport().add(panelWidget);
        //Setto la ScrollPane con un bordo vuoto
        ScrollPanelWidget.setBorder(new EmptyBorder(0,0,0,0));
        
        //Pattern strategy impostazione del tipo di layout del contenitore(pannello dei widget)
        panelWidget.setLayout(new GridBagLayout());
        //Inizializzazione ed dispongo lo status dei canvas
        inizializeStatus();
        //Creo un primo frame di default
        createFrame();
        //Inizializzo ed dispongo i controlli delle varie variabili nel pannello
        inizializeComponentVariable();
        //Inizializzo ed dispongo la tabella dei PowerSet all'interno del pannello
        inizializePanelPowerSet();
        
        
         /*Disposizione del pannello dei widget all'interno del frame*/
        GridBagConstraints constraintPanelWidget = new GridBagConstraints();
        //Inizio cella
        constraintPanelWidget.gridx=0;
        constraintPanelWidget.gridy=1;
        //Occupa tutto lo spazio disponibile
        constraintPanelWidget.weightx=1.0;
        constraintPanelWidget.weighty=1.0;
        //Si può estendere in entrambe le direzioni
        constraintPanelWidget.fill=GridBagConstraints.BOTH;
        //Orientamento verso l'alto
        constraintPanelWidget.anchor=GridBagConstraints.NORTHWEST;
        //Aggiungo il widget al pannello
        this.add(ScrollPanelWidget,constraintPanelWidget);

    }
    
    private void inizializeStatus(){
        //Inizializzo il label
        status.setText("Status");
        //Inizializzo il modello dello status
        statusModel =new DefaultListModel();
        //Creo la lista
        listStatus=new JList(statusModel);
        //Imposto lo bordo della lista
        listStatus.setBorder(BorderFactory.createRaisedSoftBevelBorder());
        listStatus.setPreferredSize(new Dimension(200,100));
        listStatus.setVisibleRowCount(5);
        
        GridBagConstraints constraintStatusVariable=new GridBagConstraints();
        constraintStatusVariable.gridx=10;
        constraintStatusVariable.gridy=3;
        //Imposto value margine superiore(top),margine sinistro(left),margine inferiore(bottom),margine destro(right)
        constraintStatusVariable.insets=new Insets(0,20,5,0);
        constraintStatusVariable.anchor=GridBagConstraints.SOUTH;
        panelWidget.add(status,constraintStatusVariable);
        
        //Layout per lo status delle operazioni
        constraintStatusVariable.gridx=10;
        constraintStatusVariable.gridy=4;
        constraintStatusVariable.gridheight=2;
        constraintStatusVariable.fill=GridBagConstraints.VERTICAL;
        //Imposto value margine superiore(top),margine sinistro(left),margine inferiore(bottom),margine destro(right)
        constraintStatusVariable.insets=new Insets(0,20,0,0);
        panelWidget.add(listStatus,constraintStatusVariable);
    }
    //Aggiorna i vari status del frame
    private void updateStatus(){
        //Inizializzo le stringhe
        //Rimuovo gli elementi precedenti
        statusModel.removeAllElements();
        //Per ogni frame in base al suo stato imposto la stringa
        for(int i=0;i<listFrame.size();i++){
            //Se lo stato ha due varibili in draw vedo il suo stato altrimenti lo setto ad non draw
            if(listFrame.get(i).getModelloVariable().getVariablesDraw().fullVariable()){
                if(listFrame.get(i).getModelloVariable().getState())
                    changeStatusCanvas(i,stringCorrect);
                else
                    changeStatusCanvas(i,stringError);
            }else
                changeStatusCanvas(i,stringNoDraw);
        }
        listStatus.revalidate();
    }
    //Metodo che in base alal stringa aggiona lo status del frame associato all'indice
    private void changeStatusCanvas(int index,String status){
        //Rimuovo prima la stringa precedente se era presente ed aggiungo la nuova
        if(statusModel.size()>index)
            statusModel.remove(index);
        //Setto lo status in base alla stringa passata in input
        statusModel.add(index, "View "+(index+1)+": "+status);
        
    }
    //Crea un nuovo Frame
    private FramePolyhedron createFrame(){
        //Se ho più di 5 frame lancio un messaggio che non ne posso avere di più
        if(listFrame.size()>=5){
             JOptionPane.showMessageDialog(PanelPolyhedrons.this,"Maximum number of frame","Constraint",JOptionPane.INFORMATION_MESSAGE);
             return null;
        }
        
        //Creo il frame associandogli un nuovo modello 
        final FramePolyhedron temp = 
	    new FramePolyhedron("View "+(listFrame.size()+1) + ": " + nomeFile,
				"Canvas "+(listFrame.size()+1),
				modelloPolyhedrons.createModelFrame());
        temp.setLocationRelativeTo(this);
        //Lo rendo visibile all'utente
        temp.setVisible(true);
        //Lo posiziono in cascade rispetto ai precedenti
        temp.setLocation((30*(listFrame.size()%5)),(30*(listFrame.size()%5)));
        //Il frame inizialmente è nello status No drawing
        changeStatusCanvas(listFrame.size(),stringNoDraw);
        
        //Aggiungo il nuovo frame alla lista 
        listFrame.add(temp);
        
        //Imposto un ascoltatore alla chiusura del Frame,il quale lo rimuove dalla lista
        temp.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent e){
                if(listFrame.size()>1){
                    //Mi procuro l'indice del frame dalla lista
                    int i=listFrame.indexOf(temp);
                    //Rimuovo il label corrispondente dal panello ed dalla lista
                    panelWidget.remove(labelDraws.remove(i));
                    //Rimuovo il frame dalla lista
                    listFrame.remove(temp);
                    //Aggiorno gli elementi dello status
                    updateStatus();
                    
                    //Per ogni variabile elimino il JCheckDraw
                    for(ComponentGraphicVariable variable:componentVariable){
                        //Rimuovo il JCheckDraw dalla lsta della variabile ed dal pannello
                        panelWidget.remove(variable.removeCheckDraw(i));
                        //Aggiorno l'interfaccia grafica
                        removeColumnDraw(i);
                    }
                    //Chiudo le risorse allocate per il frame
                    temp.dispose();
                }
                else
                    JOptionPane.showMessageDialog(PanelPolyhedrons.this,"To close this window, close the entire tab from the main window.","Constraint",JOptionPane.INFORMATION_MESSAGE);
            }
        });
        

        //Metto in posizione frontale il frame creato
        temp.toFront();

        return temp;
    }
    
    private void inizializeComponentVariable(){
        //Inizializzo i label
        inizializeLabel();
        //Inizializzo le variabili 
        createRowsComponentVariable();
    }
    
    private void removeColumnDraw(int index){
        //Imposto il layout della colonna
        GridBagConstraints constraintVariable = new GridBagConstraints();
        //Indice della riga della griglia del layout
        int y=1;
        int x=1;
        //Per ogni frame ridispongo la colonna
        for(int i=0;i<listFrame.size();i++){
            //Rinomino il nome ed il titolo del canvas
            listFrame.get(i).setTitle("View "+(i+1)+ ": " + nomeFile);
            listFrame.get(i).setName("Canvas "+(i+1));
            //Rinomino la colonna del draw
            labelDraws.get(i).setText("View "+(i+1));
            
            //Imposto value margine superiore(top),margine sinistro(left),margine superiore(bottom),margine destro(right)
            constraintVariable.insets=new Insets(0,0,0,10);
            //Layout per LabelDraw
            constraintVariable.gridx=x; //Colonna
            constraintVariable.gridy=0; //Riga
            panelWidget.add(labelDraws.get(i),constraintVariable);
            
            //Ripristino il margine destro a 0
            //Imposto value margine superiore(top),margine sinistro(left),margine superiore(bottom),margine destro(right)
            constraintVariable.insets=new Insets(0,0,0,0);
            
            for(ComponentGraphicVariable temp:componentVariable){
                constraintVariable.gridx=x;
                constraintVariable.gridy=y;
                panelWidget.add(temp.getCheckDraw(i),constraintVariable);
                y++;
            }
            //Vado alla colonna successiva
            x++;
            //Riparto dalla seconda riga
            y=1;
            
        }
        //Aggiorno il pannello con le ultime modifiche
        panelWidget.revalidate();
        panelWidget.repaint();
    }
    //Ritorna il valore minimo
    public int valueMedio(Pair<Float> valueExtreme){
        int ris=0;
        //Se ho il valore minimo ad infinito
        if(valueExtreme.getFirst().isInfinite()){
            //Se ho che il valore massimo è infinito allora ritorno 0
            if(valueExtreme.getSecond().isInfinite())
                ris=0;
            else //Altrimenti ritorno il valore massimo -50
                ris=(int)(valueExtreme.getSecond()-50);
        }else if(valueExtreme.getSecond().isInfinite()) //Se il valore minimo non è infinito ma il massimo è infinito allora setto come valore di ritorno il minimo+50
                ris=(int)(valueExtreme.getFirst()+50);
        else //Altrimenti ritorno la differenza
            ris=(int)((valueExtreme.getFirst()+valueExtreme.getSecond())/2);
        
        return ris;
    }
    
    private void inizializeLabel(){
        
        //Label dinamico draw per il primo frame
        labelDraws.add(new JLabel("View "+listFrame.size()));
        //Label fissi
        labelProject.setText("Project");
        labelFix.setText("Fix");

        //Creo la variabile per impostare il constraint dei label
        GridBagConstraints constraintLayout = new GridBagConstraints();
      
        //Imposto value margine superiore(top),margine sinistro(left),margine superiore(bottom),margine destro(right)
        constraintLayout.insets=new Insets(0,0,0,10);
        //Layout per LabelDraw
        constraintLayout.gridx=1; //Colonna
        constraintLayout.gridy=0; //Riga
        panelWidget.add(labelDraws.get(0),constraintLayout);
        //Imposto value margine superiore(top),margine sinistro(left),margine superiore(bottom),margine destro(right)
        constraintLayout.insets=new Insets(0,0,0,0);
        //Rimando delle colonne vuote per un eventuale inserimento di altri frame(per ora 5) in cui devo scegliere di fare il draw di varibili
        //Layout per labelProject 
        constraintLayout.gridx=7; //Colonna
        constraintLayout.gridy=0; //Riga
        panelWidget.add(labelProject,constraintLayout);
        
        //Layout per labelFix 
        constraintLayout.gridx=8; //Colonna
        constraintLayout.gridy=0; //Riga
        panelWidget.add(labelFix,constraintLayout);
    }
        
    private void createRowsComponentVariable(){
        //Recupero il nome delle variabili del poliedro caricato in memoria
        Set<String> setVariablePoly=modelloPolyhedrons.getAllStringVariable();
        
        //Variabili di appoggio ad ogni ciclo
        JLabel tempLabelVariable;
        JCheckBox tempCheckDraw,tempCheckProject;
        JSlider tempSliderVariable;
        JFormattedTextField tempTextVariable;
        ItemListener tempControllerVariable;
        ComponentGraphicVariable tempVariable;
        
        GridBagConstraints constraintVariable = new GridBagConstraints();
       
        //Indice della riga della griglia del layout(Parto dalla riga successiva all'ultimo elemento già presente oppure dalla riga 1 se non ci sono elementi già presenti)
        int y=1;
        for (String variable : setVariablePoly) {
            //Nome variabile
            tempLabelVariable=new JLabel(variable);
            constraintVariable.gridx=0;
            constraintVariable.gridy=y;
            panelWidget.add(new JLabel(variable),constraintVariable);
            //Tengo traccia delle variabili caricate nel pannello
            labelVariabili.add(variable);
               
            //JCheckBox per il Draw del primo frame
            tempCheckDraw=new JCheckBox();
            constraintVariable.gridx=1;
            constraintVariable.gridy=y;
            panelWidget.add(tempCheckDraw,constraintVariable);
            
            //JCheckBox per il Project
            tempCheckProject=new JCheckBox();
            constraintVariable.gridx=7; //Lascio delle colonne vuote per altri eventuali colonne di draw
            constraintVariable.gridy=y;
            panelWidget.add(tempCheckProject,constraintVariable);
            
            //JSlider per sezionare il valore
            tempSliderVariable=new JSlider(-10,10,0);
            tempSliderVariable.setPaintLabels(true);
            //tempSliderVariable.setExtent(2);
            tempSliderVariable.setMajorTickSpacing(5);
            tempSliderVariable.setMinorTickSpacing(2);
            tempSliderVariable.setPaintTicks(true);
            //Imposto value margine superiore(top),margine sinistro(left),margine inferiore(bottom),margine destro(right)
            constraintVariable.insets=new Insets(20,0,0,0);
            constraintVariable.gridx=8;
            constraintVariable.gridy=y;
            panelWidget.add(tempSliderVariable,constraintVariable);
            
            //Imposto value margine superiore(top),margine sinistro(left),margine inferiore(bottom),margine destro(right)
            constraintVariable.insets=new Insets(0,0,0,0);
            
            //JFormattedTextField per visualizzare/Impostare il valore di sezionamento
            tempTextVariable=new JFormattedTextField(0);
            tempTextVariable.setColumns(4);
            tempTextVariable.setHorizontalAlignment(JFormattedTextField.CENTER);
            constraintVariable.gridx=9;
            constraintVariable.gridy=y;
            panelWidget.add(tempTextVariable,constraintVariable);
           
            //Creo l'oggetto che rappresenta tutte le componenti grafiche di una variabile
            tempVariable=new ComponentGraphicVariable(tempLabelVariable,tempCheckDraw,tempCheckProject,tempSliderVariable,tempTextVariable);
            //Creo un osservatore che gestisce gli eventi dei vari oggetti collegati ad una variabile caricata
            tempControllerVariable=createControllerDrawProject(tempVariable);
            //Creo un osservatore che gestisce il variatore di velocita 
            tempSliderVariable.addMouseListener(createControllerSliderValue(tempVariable));
            //Creo un osservatore che gestisce il textField in base al suo cambiamento 
            tempTextVariable.addPropertyChangeListener(createControllerJtextFieldValue(tempVariable));
            //Mi salvo l'oggetto creato
            componentVariable.add(tempVariable);
            
            //Aggiungo gli osservatore appena creati agli oggetti che devono osservare rispettivamente
            tempCheckDraw.addItemListener(tempControllerVariable);
            tempCheckProject.addItemListener(tempControllerVariable);
            //Calcolo il valore medio per inizializzare il JTextField
            int valueMedio=valueMedio(modelloPolyhedrons.getUpperLowerBoundVariable(tempVariable.getLabelVariable().getText()));
            //Inizializzo value valori delle variabili con un valore medio, in caso di infinito lo setto a 0, ed l'ho imposto per tutti i frame
            tempVariable.getTextVariable().setValue(valueMedio);
            //Vado alla riga successiva
            y++;
        }
        //Inizializzazione ed posizionamento del separator
        GridBagConstraints constraintJSeparetor=new GridBagConstraints();
        constraintJSeparetor.gridx=6;
        constraintJSeparetor.gridy=0;
        constraintJSeparetor.gridheight=y;
        //Imposto value margine superiore(top),margine sinistro(left),margine inferiore(bottom),margine destro(right)
        constraintJSeparetor.insets=new Insets(0,0,0,10);
        constraintJSeparetor.fill=GridBagConstraints.VERTICAL;
        panelWidget.add(separatorWidget,constraintJSeparetor);
        
        //Inizializzazione ed posizionamento del button Add
        GridBagConstraints constraintButtonAdd = new GridBagConstraints();
        //Inserimento Button ADD per aggiungere un nuovo frame
        constraintButtonAdd.gridx=0;
        constraintButtonAdd.gridy=y;

        panelWidget.add(addWindows,constraintButtonAdd);
        
        //Creo un osservatore che crea il nuovo frame richiesto
        addWindows.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                //Creo un nuovo frame ed aggiungo la colonna corrispondente al nuovo frame
                 addColumnComponentVariable();
            }
        });
        
    }
    private void addColumnComponentVariable(){

        //Variabile d'appoggio ad ogni ciclo
        JCheckBox tempCheckDraw;
        
        //Creo il nuovo frame: ritorna null se ho raggiunto il numero massimo di frame che posso creare
        FramePolyhedron frame=createFrame();
        if(frame!=null){
            //Indice della riga della griglia del layout(Parto dalla riga successiva all'ultimo elemento già presente oppure dalla riga 1 se non ci sono elementi già presenti)
            int y=1;
            int x=listFrame.size();
            //Setto il nome della colonna
            JLabel tempLabel=new JLabel("View "+listFrame.size());
            //Lo aggiungo alla lista dei label
            labelDraws.add(tempLabel);
            //Imposto il layout della colonna
            GridBagConstraints constraintVariable = new GridBagConstraints();
            //Imposto value margine superiore(top),margine sinistro(left),margine superiore(bottom),margine destro(right)
            constraintVariable.insets=new Insets(0,0,0,10);
            //Layout per LabelDraw
            constraintVariable.gridx=x; //Colonna
            constraintVariable.gridy=0; //Riga
            panelWidget.add(tempLabel,constraintVariable);
            //Ripristino il margine destro a 0
            //Imposto value margine superiore(top),margine sinistro(left),margine superiore(bottom),margine destro(right)
            constraintVariable.insets=new Insets(0,0,0,0);
            for(ComponentGraphicVariable temp:componentVariable){
            
                //JCheckBox per il Draw associato al nuovo frame
                tempCheckDraw=new JCheckBox();
                constraintVariable.gridx=x;
                constraintVariable.gridy=y;
                panelWidget.add(tempCheckDraw,constraintVariable);
                //Lo aggiungo alla riga corrispondente
                temp.addCheckDraw(tempCheckDraw);
                //Aggiungo l'osservatore corrispondente a quella riga
                if(temp.getListener()!=null)
                    tempCheckDraw.addItemListener(temp.getListener());
                else
                    throw new RuntimeException("Controller null");
            
                //Ad ogni FrameCreato deve prendere tutti i valori globali già settati 
                if(temp.getCheckProject().isSelected()) //Se la variabile è proiettata la imposto nel frame
                    frame.getModelloVariable().setVariableProject(temp.getLabelVariable().getText());
                else{ //altrimenti mi prendo il valore sezionato ed l'imposto nel frame se esso non è sezionato
                    //Se lo stato della variabile è in errore non aggiorno il valore
                    if(temp.getStato())
                        frame.getModelloVariable().setVariableValue(temp.getLabelVariable().getText(),(Integer)temp.getTextVariable().getValue()); //Seziono il valore della variabile
                }
                //Visto che aggiungo un nuovo JcheckBox,se sto nel caso che ho disabilitato gli altri componenti perchè tutti i draw sono occupati devo ripristinare
                 temp.getCheckProject().setEnabled(true);
                //Se però il project è selezionato,devo abilitare solo lui
                if(!temp.getCheckProject().isSelected()){
                    temp.getVariatorVelocity().setEnabled(true);
                    temp.getTextVariable().setEnabled(true);
                }
                //Vado alla riga successiva
                y++;
            }
            //Aggiorno tutti i canvas
            updateCanvasFrames();
            //Aggiorno il pannello con le ultime modifiche
            panelWidget.revalidate();
            panelWidget.repaint();
        }
    }
    
    private void inizializePanelPowerSet(){
        
        labelPowerSets.setText("Sets");
        //Creo la variabile per impostare il constraint per il label
        GridBagConstraints constraintLayout = new GridBagConstraints();
        //Layout per labelPowerSets
        constraintLayout.gridx=10;
        constraintLayout.gridy=0;
        //Imposto value margine superiore(top),margine sinistro(left),margine superiore(bottom),margine destro(right)
        constraintLayout.insets=new Insets(0,20,10,0);
        panelWidget.add(labelPowerSets,constraintLayout);
        
        tablePowerSet = new JTable(new TableModel());
        
        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(tablePowerSet);
        tablePowerSet.setPreferredScrollableViewportSize(new Dimension(200, 70));
        //Set up renderer and editor for the Favorite Color column.
        tablePowerSet.setDefaultRenderer(Color.class,new ColorRenderer(true));
        tablePowerSet.setDefaultEditor(Color.class,new ColorEditor());
        TableColumn column;
        column = tablePowerSet.getColumnModel().getColumn(1);
        column.setPreferredWidth(5);
        column = tablePowerSet.getColumnModel().getColumn(0);
        column.setPreferredWidth(100);
        
        //Impostazione del layout della tabella all'interno del Panel principale
        GridBagConstraints constraintPanelPowerSet = new GridBagConstraints();
        constraintPanelPowerSet.gridx=10;
        constraintPanelPowerSet.gridy=1;
        constraintPanelPowerSet.gridheight=2;
        constraintPanelPowerSet.fill=GridBagConstraints.VERTICAL;
        
        //Orientamento verso l'alto
        constraintPanelPowerSet.anchor=GridBagConstraints.NORTHWEST;
        //Imposto value margine superiore(top),margine sinistro(left),margine superiore(bottom),margine destro(right)
        constraintPanelPowerSet.insets=new Insets(0,20,0,0);      
        panelWidget.add(scrollPane,constraintPanelPowerSet);
        
       
    }
    
    //Aggiunta di altri PowerSet tramite file
    public void addPowerSet(String nomeFile){
        //Aggiorno il modello ed dettagli dei vari frame
        updateAllFrame(nomeFile);
        //Aggiorno le variabili sul widget
        updateComponenVariable();
        //Aggiorno la visualizzazione dei frame
        updateCanvasFrames();
        
        //Aggiorno la tabella
        tablePowerSet.revalidate();
        tablePowerSet.repaint();
        //Aggiorno il pannello con le ultime modifiche
        panelWidget.revalidate();
        panelWidget.repaint();
    }

    //Aggiorno tutti i frame in base ai nuovi powerSet provenienti dal file caricato in memoria
    private void updateAllFrame(String nomeFile){
        FramePolyhedron tempFrame;
        Pair<Float> upperLowerBoundVariable;
        String variableDraw;
        PairDraw<Long> pairDraw;
        //Modifico il nome del file associato al pannello
        // nomeFile = nomeFile.concat(",("+nomeFile+")");

        for(int i=0;i<listFrame.size();i++){
            tempFrame=listFrame.get(i);
            //Aggiorno il titolo del frame
            String titleFrame = tempFrame.getTitle() + " " + nomeFile;
            tempFrame.setTitle(titleFrame);

            //Aggiorno il powerSet del modello
            listFrame.get(i).getModelloVariable().updateListPowerSet();
            
            //Recupero gli estremi della variabile
            pairDraw = tempFrame.getModelloVariable().getVariablesDraw();
            if(pairDraw!=null) {
                //Il primo asse
                if(pairDraw.getFirst()!=null){
                    variableDraw=modelloPolyhedrons.getStringVariable(pairDraw.getFirst());
                    if(variableDraw!=null){
                        upperLowerBoundVariable = modelloPolyhedrons.getUpperLowerBoundVariable(variableDraw);
                        //Comunica al pannello il cambiamento dell'asse
                        tempFrame.setVariableAxes(variableDraw,upperLowerBoundVariable.getFirst(),upperLowerBoundVariable.getSecond());
                    }
                }
                //Il secondo asse
                if(pairDraw.getSecond()!=null){
                    variableDraw=modelloPolyhedrons.getStringVariable(pairDraw.getSecond());
                    if(variableDraw!=null){
                        upperLowerBoundVariable = modelloPolyhedrons.getUpperLowerBoundVariable(variableDraw);
                        //Comunica al pannello il cambiamento dell'asse
                        tempFrame.setVariableAxes(variableDraw,upperLowerBoundVariable.getFirst(),upperLowerBoundVariable.getSecond());
                    }
                }
            } // end if
	}
    }
    
    //Creo ed aggiungo le nuove variabili provenienti dai nuovi powerSet caricati in memoria
    private void updateComponenVariable(){
        //Rimuovo il separotor il quale la sua posizione dipende dal numero delle variabili
        panelWidget.remove(separatorWidget);
        //Rimuovo il button il quale la sua posizione dipende dal numero delle variabili
        panelWidget.remove(addWindows);
        //Aggiungo le nuove variabili ed riaggiungo nella nuova posizione il pulsante 'add'
        addRowsComponentVariable();
        //Se avevo due variabili in draw,disattivo quelle appena inseriti
        int i=0;
        //Se ho due caselle già selezionate in draw disabilito le nuove inseriti
        for(FramePolyhedron tempFrame:listFrame){
            //Se il frame è full vado a disattivare quelle appena inserite
            if(tempFrame.getModelloVariable().getVariablesDraw().fullVariable())
                //Disabilito le altre caselle di draw per le altre variabili
                for(ComponentGraphicVariable temp:componentVariable){
                    JCheckBox draw=temp.getCheckDraw(i);
                    if(!draw.isSelected())
                        draw.setEnabled(false);
                }
            i++;
        }
    }
    
    private void addRowsComponentVariable(){
        //Recupero il nome delle variabili del poliedro caricato in memoria
        Set<String> setVariablePoly=modelloPolyhedrons.getAllStringVariable();
        
        //Variabili di appoggio ad ogni ciclo
        JLabel tempLabelVariable;
        List<JCheckBox> tempCheckDraws;
        JCheckBox  tempCheckProject;
        JSlider tempSliderVariable;
        JFormattedTextField tempTextVariable;
        ItemListener tempControllerVariable;
        ComponentGraphicVariable tempVariable;
        
        GridBagConstraints constraintVariable = new GridBagConstraints();
       
        //Indice della riga della griglia del layout(Parto dalla riga successiva all'ultimo elemento già presente oppure dalla riga 1 se non ci sono elementi già presenti)
        int y=componentVariable.size()+1;
        //Scorro le variabili del modello
        for (String variable : setVariablePoly) {
            //Se la variabile era già presente nel vecchio powerSet non faccio nulla
            if(!labelVariabili.contains(variable)){
                //Nome variabile
                tempLabelVariable=new JLabel(variable);
                constraintVariable.gridx=0;
                constraintVariable.gridy=y;
                panelWidget.add(new JLabel(variable),constraintVariable);
                //Tengo traccia delle variabili caricate nel pannello
                labelVariabili.add(variable);
                
                JCheckBox tempDraw;
                //Ad ogni riga avrò un numero di JCheckDraw in base al numero dei frame
                tempCheckDraws=new ArrayList<>();
                //Per ogni frame creo un JCheckBox
                for(int x=1;x<=listFrame.size();x++){
                    tempDraw=new JCheckBox();
                    //JCheckBox per il Draw
                    tempCheckDraws.add(tempDraw);
                    constraintVariable.gridx=x;
                    constraintVariable.gridy=y;
                    panelWidget.add(tempDraw,constraintVariable);
                }
            
                //JCheckBox per il Project
                tempCheckProject=new JCheckBox();
                constraintVariable.gridx=7; //Lascio delle colonne vuote per altri eventuali colonne di draw
                constraintVariable.gridy=y;
                panelWidget.add(tempCheckProject,constraintVariable);
            
                //JSlider per sezionare il valore
                tempSliderVariable=new JSlider(-10,10,0);
                tempSliderVariable.setPaintLabels(true);
                //tempSliderVariable.setExtent(2);
                tempSliderVariable.setMajorTickSpacing(5);
                tempSliderVariable.setMinorTickSpacing(2);
                tempSliderVariable.setPaintTicks(true);
                //Imposto value margine superiore(top),margine sinistro(left),margine inferiore(bottom),margine destro(right)
                constraintVariable.insets=new Insets(20,0,0,0);
                constraintVariable.gridx=8;
                constraintVariable.gridy=y;
                panelWidget.add(tempSliderVariable,constraintVariable);
            
                //Imposto value margine superiore(top),margine sinistro(left),margine inferiore(bottom),margine destro(right)
                constraintVariable.insets=new Insets(0,0,0,0);
            
                //JFormattedTextField per visualizzare/Impostare il valore di sezionamento
                tempTextVariable=new JFormattedTextField(0);
                tempTextVariable.setColumns(4);
                tempTextVariable.setHorizontalAlignment(JFormattedTextField.CENTER);
                constraintVariable.gridx=9;
                constraintVariable.gridy=y;
                panelWidget.add(tempTextVariable,constraintVariable);
           
                //Creo l'oggetto che rappresenta tutte le componenti grafiche di una variabile
                tempVariable=new ComponentGraphicVariable(tempLabelVariable,tempCheckDraws,tempCheckProject,tempSliderVariable,tempTextVariable);
                //Creo un osservatore che gestisce gli eventi dei vari oggetti collegati ad una variabile caricata
                tempControllerVariable=createControllerDrawProject(tempVariable);
                //Creo un osservatore che gestisce il variatore di velocita 
                tempSliderVariable.addMouseListener(createControllerSliderValue(tempVariable));
                //Creo un osservatore che gestisce il textField in base al suo cambiamento 
                tempTextVariable.addPropertyChangeListener(createControllerJtextFieldValue(tempVariable));
                //Mi salvo l'oggetto creato
                componentVariable.add(tempVariable);
            
                //Aggiungo gli osservatore appena creati agli oggetti che devono osservare rispettivamente
                for(JCheckBox temp:tempCheckDraws)
                    temp.addItemListener(tempControllerVariable);
                
                tempCheckProject.addItemListener(tempControllerVariable);
                //Calcolo il valore medio per inizializzare il JTextField
                int valueMedio=valueMedio(modelloPolyhedrons.getUpperLowerBoundVariable(tempVariable.getLabelVariable().getText()));
                //Inizializzo value valori delle variabili con un valore medio, in caso di infinito lo setto a 0, ed l'ho imposto per tutti i frame
                tempVariable.getTextVariable().setValue(valueMedio);
                //Vedo se il valore non è nel range della variabile
                if(!modelloPolyhedrons.isVariableBox(modelloPolyhedrons.getIndiceVariable(tempVariable.getLabelVariable().getText()),valueMedio)){ //caso in cui il valore non rispetta il range
                    //Imposto il background del testo a rosso
                    tempVariable.getTextVariable().setBackground(Color.red);
                    //Metto lo stato a false
                    tempVariable.setStato(false);
                }else//Caso in cui il valore rispetta il range
                    for(FramePolyhedron tempFrame:listFrame)
                        tempFrame.getModelloVariable().setVariableValue(tempVariable.getLabelVariable().getText(), valueMedio);//Salvo nel modello il valore della variabile da sezionare
                //Vado alla riga successiva
                y++;
            }
        }
      
        //Inizializzazione ed posizionamento del separator
        GridBagConstraints constraintJSeparetor=new GridBagConstraints();
        constraintJSeparetor.gridx=6;
        constraintJSeparetor.gridy=0;
        constraintJSeparetor.gridheight=y;
        //Imposto value margine superiore(top),margine sinistro(left),margine inferiore(bottom),margine destro(right)
        constraintJSeparetor.insets=new Insets(0,0,0,10);
        constraintJSeparetor.fill=GridBagConstraints.VERTICAL;
        panelWidget.add(separatorWidget,constraintJSeparetor);
        
        //Inizializzazione ed posizionamento del button Add
        GridBagConstraints constraintButtonAdd = new GridBagConstraints();
        //Inserimento Button ADD per aggiungere un nuovo frame
        constraintButtonAdd.gridx=0;
        constraintButtonAdd.gridy=y;

        panelWidget.add(addWindows,constraintButtonAdd);
        
    }
    
    public void disposeAllFrames(){
	for (FramePolyhedron fp: listFrame)
	    fp.dispose();
    }

    //Metodo che aggiorna i canvas dei vari frame
    public void updateCanvasFrames(){
        //Indice per lo status
        int i=0;
        //Per ogni frame vedo che se le variabili Draw sono due comunico al modello variamenti delle proezioni ed se non è in errore
        for(FramePolyhedron tempFrame:listFrame){
            //Se ho due variabili in draw vado a disegnare
            if(tempFrame.getModelloVariable().getVariablesDraw().fullVariable()){
                //Comunico al modello che può preparare value dati del poliedro da visualizzare
                try{
                    //Se il metodo ritorna true,allora vuol dire che l'impostazioni dell'utente vanno bene
                    if(modelloPolyhedrons.drawPolyhedrons(tempFrame.getModelloVariable()))
                        changeStatusCanvas(i,stringCorrect);
                    else{
                        changeStatusCanvas(i,stringError); //altrimenti ho il canvas vuoto
                        //Resetto eventualmente la lista
                        tempFrame.resetListPolyhedronNNC();
                    }
                }catch(RuntimeException e){
		    System.out.println(e);
                    //Metto lo status a false,perchè si è realizzato un errore
                    changeStatusCanvas(i,this.stringError);
                    //Resetto eventualmente la lista
                    tempFrame.resetListPolyhedronNNC();
                }
            }else{
                changeStatusCanvas(i,this.stringNoDraw); //Caso in cui non ho due variabili in draw,quindi resetto il canvas
                //Resetto eventualmente la lista
                tempFrame.resetListPolyhedronNNC();
            }
            i++; //Incremento l'indice dello status
        }
    }
    
    //Metodo che aggiorna il canvas del frame dato in input
    public void updateCanvasFrame(FramePolyhedron canvasFrame,int indexFrame){
        //Se ho due variabili in draw vado a disegnare
        if(canvasFrame.getModelloVariable().getVariablesDraw().fullVariable()){
            //Comunico al modello che può preparare value dati del poliedro da visualizzare
            try{
                //Se il metodo ritorna true,allora vuol dire che l'impostazioni dell'utente vanno bene
                if(modelloPolyhedrons.drawPolyhedrons(canvasFrame.getModelloVariable()))
                    changeStatusCanvas(indexFrame,stringCorrect);
                else{
                    changeStatusCanvas(indexFrame,stringError);//altrimenti ho il canvas vuoto
                    //Resetto eventualmente la lista
                    canvasFrame.resetListPolyhedronNNC();
                }
                    
            }catch(RuntimeException e){
		System.out.println(e);
		// e.printStackTrace(); // DEBUG
                //Metto lo status a false,perchè si è realizzato un errore
                changeStatusCanvas(indexFrame,stringError);
                //Resetto eventualmente la lista
                canvasFrame.resetListPolyhedronNNC();
            }
        }else{
            changeStatusCanvas(indexFrame,stringNoDraw);//Caso in cui non ho due variabili in draw,quindi resetto il canvas
            //Resetto eventualmente la lista
            canvasFrame.resetListPolyhedronNNC();
        }
    }
    
    
    /*Casi gestiti nell'oggetto osservatore ControllerDrawProject nella cascada if-else*/
    /*Vedo value vari casi:
                    1* Se l'evento è stato generato da checkDraw:
                       1.1*Se è stato selezionato devo incrementare l'indice e vedo se sto nel caso in cui è >=2 vuol dire che 
                           ho raggiunto le due variabili da disegnare ed devo disattivare le altre in modo da non poter essere
                           selezionate per disegnare
                           *Ed disattivo gli altri elementi riguardanti la variabile
                       1.2*Altrimenti vuol dire che è stato deselezionato,allora controllo se indexDraw era >=2 devo poter permettere
                           di selezionare le altre variabili per essere disegnate ed però devo controllare se queste sono selezionate 
                           ad essere proiettate prima di abilitare il check di draw
                           Attivo poi comunque le altre opzioni per la variabile
                    2* Se l'evento è stato generato da checkProject:
                       2.1*Se è stato selezionato devo disattivare le altre opzioni per la variabile
                       2.2*Se è stato deselezionato devo poter permettere le altre opzioni però devo controllare che se l'indice delle 
                           variabili che posso disegnare ha già raggiunto il limite non posso attivare il draw per la variabile
                */
    
    private ItemListener createControllerDrawProject(final ComponentGraphicVariable variable){
        
        class ControllerDrawProject implements ItemListener{
            private final ComponentGraphicVariable variable;
 
            //Mi salvo oggetto rappresentate value componenti della stessa variabile
            public ControllerDrawProject(ComponentGraphicVariable variable) {
                this.variable =variable;
            }
            
            @Override
            public void itemStateChanged(ItemEvent event) {
                //Mi salvo la sorgente dell'evento    
                Object source=event.getSource();
                
                Pair<Float> upperLowerBoundVariable;
                
                //Mi procuro l'indice della variabile della riga presa in considerazione
                long indexVariable=modelloPolyhedrons.getIndiceVariable(variable.getLabelVariable().getText());
                //Mi procuro l'indice della casella di JCheckBox che ha scatenato l'evento(se l'indice è -1 vuol dire che non ha trovato nulla)
                //Quindi abbiamo che l'evento è stato scatenato da un JcheckBox associato a project oppure si è verificato un errore non verificato
                int indexDraw=variable.getIndexCheckDraw(source);
                
                //Vedo chi ha generato l'evento
                if(source.equals(variable.getCheckDraw(indexDraw))){
                    
                    //Mi procuro il canvas associato alla colonna a cui appartiene il JcheckBox che ha scatenato l'evento
                    FramePolyhedron selectionFrame = listFrame.get(indexDraw);
                    if(selectionFrame==null)
                        throw new RuntimeException("Frame selezionato null");
               
                    //Mi procuro il modello del frame selezionato
                    ModelFrame modelloPoliedroCanvas=selectionFrame.getModelloVariable();
                    
                    //Caso in cui ha generato l'evento il checkDraw ed vedo se è stato selezionato oppure rimosso
                    if(variable.getCheckDraw(indexDraw).isSelected()){
                        //Comunica al modello quale variabile è stata selezionata
                        modelloPoliedroCanvas.setVariableDraw(variable.getLabelVariable().getText());
                        //Recupero gli estremi della variabile
                        upperLowerBoundVariable = modelloPolyhedrons.getUpperLowerBoundVariable(variable.getLabelVariable().getText());
                        //Comunica al pannello la variabile che è stata selezionata con i rispettivi limiti di valori
                        selectionFrame.setVariableAxes(variable.getLabelVariable().getText(),upperLowerBoundVariable.getFirst(),upperLowerBoundVariable.getSecond());
                        //Resetto il valore di sezionamento(se esso era stato inpostato)
                        modelloPoliedroCanvas.unSetVariableValue(variable.getLabelVariable().getText());
                        //Rimuovo nella lista project se era inserita
                        modelloPoliedroCanvas.unSetVariableProject(variable.getLabelVariable().getText());
                        
                        //Se ho due caselle già selezionate in draw disabilito le restanti
                        if(modelloPoliedroCanvas.getVariablesDraw().fullVariable()){
                            //Disabilito le altre caselle di draw per le altre variabili
                            for(ComponentGraphicVariable temp:componentVariable){
                                JCheckBox draw=temp.getCheckDraw(indexDraw);
                                if(!draw.isSelected())
                                    draw.setEnabled(false);
                            }
                            //aggiorno il canvas del frame
                            updateCanvasFrame(selectionFrame,indexDraw);
                        }
                        
                    }//Caso in cui è stato rimosso il check da draw
                    else{
                        //Vedo che se avevo due variabili in draw devo riattivare gli altri Draw
                        if(modelloPoliedroCanvas.getVariablesDraw().fullVariable()){
                            //Devo attivare gli altri checkDraw disabilitati in precedenza
                            for(ComponentGraphicVariable temp:componentVariable)
                                temp.getCheckDraw(indexDraw).setEnabled(true);
                            
                            //Metto lo status a non drawing
                             changeStatusCanvas(indexDraw,stringNoDraw);
                        }
                        
                        //Comunica al pannello quale variabile non è più destinata ad essere disegnata
                        selectionFrame.unSetVariableAxes(variable.getLabelVariable().getText());
                        //Comunica al modello quale variabile non è più destinata ad essere disegnata
                        modelloPoliedroCanvas.unSetVariableDraw(variable.getLabelVariable().getText());
                            
                        //Vedo se globalmente è proiettata o sezionata
                        if(!variable.getCheckProject().isSelected()){
                            //Vedo se ha un valore valido di sezionamento
                            if(variable.getStato()!=false)
                                modelloPoliedroCanvas.setVariableValue(variable.getLabelVariable().getText(),(Integer)variable.getTextVariable().getValue()); //Setto il valore di sezionamento globale
                        }
                        else
                            modelloPoliedroCanvas.setVariableProject(variable.getLabelVariable().getText()); //La proietto
                    }
                    
                    //Abilito disabilito i vari componenti nella stessa riga
                    enableRow(variable);
                }//Vedo se l'oggetto che ha scatenato l'evento è associato a project
                else if(variable.getCheckProject().equals(source)){ 
                    //Vedo se è stata selezionata 
                    if(variable.getCheckProject().isSelected()){
                        //Per ogni frame vedo che se la variabile non è in draw la proietto
                        for(FramePolyhedron tempFrame:listFrame){
                            if(!tempFrame.getModelloVariable().isVariableDraw(indexVariable)){
                                //Comunicare la variabile che è stata selezionata per la proezione
                                tempFrame.getModelloVariable().setVariableProject(variable.getLabelVariable().getText());
                                //Resetto il valore di sezionamento(se esso era stato impostato)
                                tempFrame.getModelloVariable().unSetVariableValue(variable.getLabelVariable().getText());
                            }//Se è in draw non faccio nulla
                        }
                        //Disabilito le altre opzioni per la variabile
                        variable.getVariatorVelocity().setEnabled(false);
                        variable.getTextVariable().setEnabled(false);
                    }
                    else//Se è stata deselezionata
                    {
                        //Per ogni frame vedo che se la variabile non è in draw la deve togliere dal project
                        for(FramePolyhedron tempFrame:listFrame){
                            if(!tempFrame.getModelloVariable().isVariableDraw(indexVariable)){
                                //Comunicare la variabile che è stata deselezionata per la proezione
                                tempFrame.getModelloVariable().unSetVariableProject(variable.getLabelVariable().getText());
                                //Setto il valore di sezionamento che era stato impostato precedentemente, se aveva un valore valido
                                if(variable.getStato()!=false)
                                    tempFrame.getModelloVariable().setVariableValue(variable.getLabelVariable().getText(),(Integer)variable.getTextVariable().getValue());
                            }
                          
                        }
                        //Abilito di nuovo la possibilità di sezionare
                        variable.getVariatorVelocity().setEnabled(true);
                        variable.getTextVariable().setEnabled(true);
                    }
                    //Aggiorno tutti i canvas in base alle modifiche
                    updateCanvasFrames();
                }
                else //Se l'origine dell'evento non è uguale nè al project e ne al draw si è verificata un eccezione non verificata 
                    throw new RuntimeException("Nessun caso verificato,errore generazione eventi");
            }//end Method
        }//end class
        //Ritorno il nuovo controller
        return new ControllerDrawProject(variable);
    }
    
    private void enableRow(ComponentGraphicVariable row){
        
        //Se ho tutte i draw selezionati nella stessa riga allora disabilito il project ed il value
        boolean ok=false;
        Iterator<JCheckBox> iterator=row.getCheckDraws().iterator();
        while(iterator.hasNext() && ok!=true){
            if(!iterator.next().isSelected())
                ok=true;
        }
        if(ok==false){
            row.getCheckProject().setEnabled(false);
            row.getTextVariable().setEnabled(false);
            row.getVariatorVelocity().setEnabled(false);
        }
        else
        {
            row.getCheckProject().setEnabled(true);
            if(!row.getCheckProject().isSelected()){
                row.getTextVariable().setEnabled(true);
                row.getVariatorVelocity().setEnabled(true);
            }
        }
        
    }

    private PropertyChangeListener createControllerJtextFieldValue(final ComponentGraphicVariable variable){
        
        class ControllerJtextFieldValue implements PropertyChangeListener{
            private ComponentGraphicVariable variable;

            //Mi salvo oggetto rappresentate value componenti della stessa variabile
            public ControllerJtextFieldValue(ComponentGraphicVariable variable) {
                this.variable =variable;
            }
            
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                //Mi procuro il valore della variabile
                int value=(Integer)variable.getTextVariable().getValue();
                //Mi procuro l'indice della variabile
                long indexVariable=modelloPolyhedrons.getIndiceVariable(variable.getLabelVariable().getText());
                //Variabile d'appoggio per il modello del frame
                ModelFrame modelloPoliedro;
                //Per poter sezionare per prima cosa vedo se la variabile non è proiettata ed che sia un valore valido
                if(!variable.getCheckProject().isSelected() && variable.getTextVariable().isEditValid()){
                    //Per decidere a quale frame devo applicare il sezionamento devo vedere se questo non è in draw
                    //Scorro tutti i JCheckBox
                    for(JCheckBox draw:variable.getCheckDraws()){
                        //Se il draw non è selezionato applico il sezionamento
                        if(!draw.isSelected()){
                            //Vedo se il valore non è nel range della variabile
                            if(!modelloPolyhedrons.isVariableBox(indexVariable,value)){ //caso in cui il valore non rispetta il range
                                
                                //Se lo stato della variabile è a true,la setto a false ed applico le modifiche altrimenti non faccio nulla
                                if(variable.getStato()==true){
                                    variable.setStato(false);
                                    //Imposto il background del testo a rosso
                                    variable.getTextVariable().setBackground(Color.red);
                                    
                                    //Per ogni frame vedo se la variabile è in draw,se non lo è allora resetto il canvas ed metto lo status EmptyPolyhedron
                                    int i=0;
                                    for(FramePolyhedron tempFrame:listFrame){
                                        if(!tempFrame.getModelloVariable().isVariableDraw(indexVariable)){
                                            //Se il frame era in Draw resetto il canvas
                                            if(tempFrame.getModelloVariable().isDraw()){
                                                //Inserisco l'errore per quel frame
                                                changeStatusCanvas(i,stringError);
                                                //Resetto il frame di visualizzazione
                                                tempFrame.resetListPolyhedronNNC();
                                            }
                                            //Rimuovo se c'èra il valore di sezionamento precedente
                                            tempFrame.getModelloVariable().unSetVariableValue(variable.getLabelVariable().getText());
                                        }
                                    }
                                }
                                //Se lo stato della variabile è a false già ha un valore di sezionamento non valido
                            }else{//Caso in cui il valore rispetta il range
                                //Se lo stato è a false allora tolgo l'errore per quella variabile
                                if(variable.getStato()==false){
                                    //Metto lo stato a true
                                    variable.setStato(true);
                                    //Ripristino il colore
                                    variable.getTextVariable().setBackground(Color.white);
                                }
                                int i=0; //Indice per lo status
                                //Seziono semplicemente dove è possibile
                                //Per ogni frame vedo se la variabile è in draw,se non lo è allora la seziono
                                for(FramePolyhedron tempFrame:listFrame){
                                    modelloPoliedro=tempFrame.getModelloVariable();
                                    if(!modelloPoliedro.isVariableDraw(indexVariable)){
                                        modelloPoliedro.setVariableValue(variable.getLabelVariable().getText(), value);  //Salvo nel modello il valore della variabile da sezionare
                                        //Se le variabili Draw sono due comunico al modello value variamenti del sezionamento
                                        updateCanvasFrame(tempFrame,i);
                                    }
                                    i++;
                                }
                            }
                        }//end if for draw selected
                    }//end for JCheckBox
                }//end if correct
            }//end method
        }//Chiudo classe
            
        return new ControllerJtextFieldValue(variable);
    }
    
    private MouseListener createControllerSliderValue(final ComponentGraphicVariable variable){
        
        class ControllerSliderValue implements MouseListener{
            private ComponentGraphicVariable variable;
            private Thread changeValueJtextField;
            
            //Mi salvo oggetto rappresentate value componenti della stessa variabile
            public ControllerSliderValue(ComponentGraphicVariable variable) {
                this.variable =variable;
            }
            
            @Override
            public void mouseClicked(MouseEvent e) {  
            }

            @Override
            public void mousePressed(MouseEvent e) {
                
                changeValueJtextField=new Thread(new Runnable(){
                    
                    @Override
                    public void run(){
                        int value; 
                        value=(Integer)variable.getTextVariable().getValue();
                        
                        try {
                                Thread.sleep(700);
                            }catch (InterruptedException ex) {
                                return;
                        }
                        //Fin quando non viene interrotto
                        while(Thread.currentThread().isInterrupted()!=true){
                            
                            //Incremento il valore del text field in base alla slider
                            value+=variable.getVariatorVelocity().getValue();
                            //Setto il JTextField
                            variable.getTextVariable().setValue(value);
                            
                            try {
                                Thread.sleep(700);
                            }catch (InterruptedException ex) {
                                return;
                            }
                        } 
                    }
                });
                changeValueJtextField.start();
            }
            
            @Override
            public void mouseReleased(MouseEvent e) {
                variable.getVariatorVelocity().setValue(0);
                changeValueJtextField.interrupt();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        }
        return new ControllerSliderValue(variable);
    }
    
    //Modello che gestisce la tabella dei PowerSet
    public class TableModel extends AbstractTableModel{
        private String[] columnNames = {"Name","Color"};

        @Override
        public int getRowCount() {
           return modelloPolyhedrons.getPolyhedrons().size();
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                PowerSet powerSet=modelloPolyhedrons.getPolyhedrons().get(rowIndex);
                if(columnIndex==0)
                    return powerSet.getNomePowerSet();
                else
                    return powerSet.getColorPowerSet(); 
        }
        @Override
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }
        
        @Override
        public String getColumnName(int col) {
            return columnNames[col];
        }

        @Override
        public boolean isCellEditable(int row, int col) {
            if (col < 1) {
                return false;
            } else {
                return true;
            }
        }

        @Override
        public void setValueAt(Object value, int row, int col) {
            PowerSet powerSet=modelloPolyhedrons.getPolyhedrons().get(row);
            //Vedere di fare il cast in sicurezza di value a color
            powerSet.setColorPowerSet((Color)value);
            updateCanvasFrames();
            
            fireTableCellUpdated(row, col);
        }

    
    }
    
    public PPLModel getModelloPolyhedrons() {
        return modelloPolyhedrons;
    }
}
    
    




        





        
            
