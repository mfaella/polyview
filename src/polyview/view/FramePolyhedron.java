package polyview.view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import static java.awt.event.KeyEvent.VK_ADD;
import static java.awt.event.KeyEvent.VK_DOWN;
import static java.awt.event.KeyEvent.VK_LEFT;
import static java.awt.event.KeyEvent.VK_RIGHT;
import static java.awt.event.KeyEvent.VK_SUBTRACT;
import static java.awt.event.KeyEvent.VK_UP;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Observable;
import java.util.Observer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import polyview.model.PPLModel.ModelFrame;

/**
 *
 * @author nello
 */
public class FramePolyhedron extends JFrame implements Observer{
    
    //Componenti per lo zoom
    private ZoomComponent sliderZoom;
    private JButton zoomIn;
    private JButton zoomOut;
    // private final static int zoomSliderMin = 0, zoomSliderMax = 100, zoomSliderOffset = 10;
    //private final float[] corrispondenzaSlider={0.1f,1.5f,2f,2.3f,2.5f,2.8f,3f,3.5f,4f};
    
     //Componenti per il translate
    private JButton moveIncrementX;
    private JButton moveIncrementY;
    private JButton moveDecrementX;
    private JButton moveDecrementY;
    
    //Componente per il salvataggio del poliedro
    private JButton savePowerSets;
    private JFileChooser browsing;
    
    //Canvas OpenGl
    private final CanvasPolyhedron panelCanvasPoly;
    
    //Variabili di gestione del canvas
    private static final int PANEL_WIDTH = 550;  // width del drawable
    private static final int PANEL_HEIGHT = 450; // height del drawable
    
    private final ModelFrame modelListPowerSet;
    
    public FramePolyhedron(String title,String name,ModelFrame modello){
        if(title==null || name==null || modello==null)
            new NullPointerException();
        
        //Imposto il nome della finestra
        setTitle(title);
        setName(name);
        //Mi salvo il modello
        modelListPowerSet=modello;
        //Aggiungo il frame creato come osservatore al modello
        modelListPowerSet.addObserver(FramePolyhedron.this);
        
        //Trovare una soluzione più elegante per la dimensione del frame iniziale(vuoto)
        //this.setPreferredSize(new Dimension(1000,1000));
        setSize(900,800);

        //Inizializzo value componenti
        initComponents();
        
        //Imposto che il frame non viene chiuso subito ma gestisco l'evento con un controller
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        
                /*Inserimento e impostazione disposizione all'interno del frame principale*/
        //Pattern strategy impostazione del layout del panel
        setLayout(new GridBagLayout());
        
        /*Creazione e disposizione del panelPoly all'interno del JPanel*/
        panelCanvasPoly = initCanvasPoly();

        GridBagConstraints constraintCanvas = new GridBagConstraints();
        //Inizio cella
        constraintCanvas.gridx=1;
        constraintCanvas.gridy=0;
        //Attributi per il ridimensionamento/ingrandimento
        constraintCanvas.weightx=1.0;
        constraintCanvas.weighty=1.0;
        
        //Imposto il numero di griglie che può occupare in width ed in height
        constraintCanvas.gridwidth=4;
        constraintCanvas.gridheight=5;
        
        //Imposto che si può espandere sia orizzontalmente che verticalmente
        constraintCanvas.fill = GridBagConstraints.BOTH;
        
        //Imposto value margine superiore(top)
        constraintCanvas.insets.top=5;
        //Aggiungo il canvas al pannello
        add(panelCanvasPoly,constraintCanvas);
        
        /*Disposizione degli strumenti di Zoom,save ed traslazione all'interno del frame*/
        GridBagConstraints constraintWidgetZoom = new GridBagConstraints();
        constraintWidgetZoom.gridx=5;
        constraintWidgetZoom.gridy=2;
        constraintWidgetZoom.anchor=GridBagConstraints.NORTH;
        add(zoomIn,constraintWidgetZoom);
        
        constraintWidgetZoom.gridy=3;
        add(sliderZoom.getSlider(), constraintWidgetZoom);
	sliderZoom.setPanel(panelCanvasPoly);
        
        constraintWidgetZoom.gridy=4;
        add(zoomOut,constraintWidgetZoom);
        
        GridBagConstraints constraintWidgetSave = new GridBagConstraints();
        constraintWidgetSave.gridx=5;
        constraintWidgetSave.gridy=4;
        add(savePowerSets,constraintWidgetSave);
        
        GridBagConstraints constraintWidgetIncrement = new GridBagConstraints();
        constraintWidgetIncrement.gridx=1;
        constraintWidgetIncrement.gridy=5;
        add(moveDecrementX,constraintWidgetIncrement);
        
        constraintWidgetIncrement.gridx=0;
        constraintWidgetIncrement.gridy=4;
        constraintWidgetIncrement.anchor=GridBagConstraints.SOUTH;
        add(moveDecrementY,constraintWidgetIncrement);
        
        constraintWidgetIncrement.gridx=4;
        constraintWidgetIncrement.gridy=5;
        constraintWidgetIncrement.anchor=GridBagConstraints.EAST;
        add(moveIncrementX,constraintWidgetIncrement);
        
        constraintWidgetIncrement.gridx=0;
        constraintWidgetIncrement.gridy=0;
        constraintWidgetIncrement.anchor=GridBagConstraints.NORTH;
        constraintWidgetIncrement.insets.top=5;
        add(moveIncrementY,constraintWidgetIncrement);
    }
   
    
    //Posiziona il frame al centro dello schermo 
    private void centerWindow(){
        //Centro il frame sullo schermo
        Dimension dim=getToolkit().getScreenSize();
        this.setLocation(dim.width/2 -this.getWidth()/2, dim.height/2 - this.getHeight()/2);
    }
    public ModelFrame getModelloVariable(){
        return this.modelListPowerSet;
    }
    
    private void initComponents(){
        //Inizializzo il file Chooser 
        browsing=new JFileChooser();
        browsing.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        browsing.setDialogTitle("Selects directory save ");
        browsing.setApproveButtonText("Select");
        
        //Buttons per Incrementare/decrementare il primo asse
        Icon image=new ImageIcon(getClass().getResource("images/arrowRight.png"));
        moveIncrementX=new JButton(image);
        moveIncrementX.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                panelCanvasPoly.decrementX();
            }
        });
        image=new ImageIcon(getClass().getResource("images/arrowLeft.png"));
        moveDecrementX=new JButton(image);
        moveDecrementX.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                panelCanvasPoly.incrementX();
            }
        });
        
        //Buttons per Incrementare/decrementare il secondo asse
        image=new ImageIcon(getClass().getResource("images/arrowUp.png"));
        moveIncrementY=new JButton(image);
        moveIncrementY.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                panelCanvasPoly.decrementY();
            }
        });
        image=new ImageIcon(getClass().getResource("images/arrowDown.png"));
        moveDecrementY=new JButton(image);
        moveDecrementY.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                panelCanvasPoly.incrementY();
            }
        });
        
	// Zoom controller
        sliderZoom = new ZoomComponent();
 
	image = new ImageIcon(getClass().getResource("images/more.png"));
        zoomIn = new JButton(image);
        zoomIn.addActionListener(sliderZoom.zoomInActionListener());
        
        image=new ImageIcon(getClass().getResource("images/less.png"));
        zoomOut=new JButton(image);
        zoomOut.addActionListener(sliderZoom.zoomOutActionListener());

	// Saving controller        
        image=new ImageIcon(getClass().getResource("images/save.png"));
        savePowerSets=new JButton(image);
        savePowerSets.addActionListener(new ActionListener(){
            int indexFile=1;
            @Override
            public void actionPerformed(ActionEvent e) {
                Path newPath;
                //Vedo per prima cosa se il modello è predisposto per disegnare ed non ha errori
                if(modelListPowerSet.getState() && modelListPowerSet.isDraw()){
                    if(browsing.showOpenDialog(FramePolyhedron.this)!=JFileChooser.APPROVE_OPTION)
                        JOptionPane.showMessageDialog(FramePolyhedron.this,"Nessuna directory selezionata","Directory Error",JOptionPane.ERROR_MESSAGE);
                    else{
                        try {
                            //Creo il file partendo dal Path che mi è stato dato aggiungendo il nome del file da creare
                            newPath = Files.createFile(Paths.get(browsing.getSelectedFile().getAbsolutePath().concat("/"+FramePolyhedron.this.getTitle().trim()+"<"+indexFile+">.poly")));
                            //Incremento l'indice che mi indica il numero dei file
                            indexFile++;
                            modelListPowerSet.savePowerSet(newPath);//Passo il path selezionato dall'utente dove verrà creato il file di scrittura
                        }catch (IOException ex) {
                            //Decremento l'indice del file
                            indexFile--;
                            try {
                                //Nel caso il file già esiste lo sovrascrivo
                                modelListPowerSet.savePowerSet(Paths.get(browsing.getSelectedFile().getAbsolutePath().concat("/"+FramePolyhedron.this.getTitle().trim()+"<"+indexFile+">.poly")));//Passo il path selezionato dall'utente dove verrà creato il file di scrittura
                            } catch (IOException ex1) {
                                JOptionPane.showMessageDialog(FramePolyhedron.this,"Error file","File Error",JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }
                }
                else
                    JOptionPane.showMessageDialog(FramePolyhedron.this,"Empty Canvas","Information",JOptionPane.INFORMATION_MESSAGE);
            }
        });
        
        
    }
    
    public void setShowPatches(boolean shoPatches){
        panelCanvasPoly.setShowPatches(shoPatches);
    }
    public void setVariableAxes(String variable,float minValue,float maxValue){
        panelCanvasPoly.setVariableAxes(variable, minValue, maxValue);
    }
    
    public void unSetVariableAxes(String variable){
        //Tolgo dal pannello la variabile
        panelCanvasPoly.unSetVariableAxes(variable);
        //Se l'indice delle variabili è maggiore di due allora resetto l'area di visualizzazione del panelCanvas
        if(modelListPowerSet.isDraw())
            panelCanvasPoly.setListPolyhedronNNC(null);
        
        //Aggiorno il display
        panelCanvasPoly.display();
        
    }
    public void display() {
        panelCanvasPoly.display();
        
    }
    public void resetListPolyhedronNNC(){
        if(modelListPowerSet.getVariablesDraw().fullVariable())
            panelCanvasPoly.setListPolyhedronNNC(null);
        //Aggiorno il display
        panelCanvasPoly.display();
        
    }
    
    private CanvasPolyhedron initCanvasPoly(){
        CanvasPolyhedron tempCanvas=new CanvasPolyhedron();
        tempCanvas.setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
        
        //Aggiungo il controllo dei tasti
        tempCanvas.addKeyListener(new KeyListener(){
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
   
                switch (e.getKeyCode()) {
		case VK_LEFT:  // player pans left (scene moves right)
		    panelCanvasPoly.incrementX();
		    break;
		case VK_RIGHT:
		    panelCanvasPoly.decrementX();
		    break;
		case VK_UP: 
		    panelCanvasPoly.decrementY();
		    break;
		case VK_DOWN:
		    panelCanvasPoly.incrementY();
		    break;
		case VK_ADD:   // "plus" key means "zoom in"
		    sliderZoom.zoomIn();
		    break;
		case VK_SUBTRACT:
		    sliderZoom.zoomOut();
		    break;
                }
            }
        
            @Override
            public void keyReleased(KeyEvent e) {
            }
            
        });
        tempCanvas.setFocusable(true);
        tempCanvas.requestFocus();
        
        return tempCanvas;
    }
    @Override
    public String toString(){
        return getName();
    }

    @Override
    public void update(Observable o, Object arg) {
        panelCanvasPoly.setListPolyhedronNNC(modelListPowerSet.getPowerSetView());
        if(modelListPowerSet.isDraw()){
           panelCanvasPoly.display();
        }
    }
}
