package polyview.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JSlider;
import java.awt.Component;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class ZoomComponent
{
    private int min = 0, max = 20, offset = 8;
    private static final float zoomFactor = 1.4f; 
    private CanvasPolyhedron panel;
    private JSlider slider;

    public ZoomComponent()
    {
        slider = new JSlider(JSlider.VERTICAL, min, max, (max-min)/2);
	slider.addChangeListener(changeListener());
    }

    public void setPanel(CanvasPolyhedron panel)
    {
	this.panel = panel;
    }

    private ChangeListener changeListener() 
    {
	return new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
		float newZoom = (float) Math.pow(zoomFactor, slider.getValue()-offset);
		panel.setZoom(newZoom);
            }
        };
    }


    public void zoomIn()
    {
	if (slider.getValue() < max)
	    slider.setValue(slider.getValue()+1);
	else {
	    max++;
	    min++;
	    offset++;
	    slider.setMaximum(max);
	    slider.setValue(max);
	    slider.setMinimum(min);
	}                 
    } 

    public void zoomOut()
    {
	if (slider.getValue() > min)
	    slider.setValue(slider.getValue()-1);
	else {
	    max--;
	    min--;
	    offset--;
	    slider.setMinimum(min);
	    slider.setValue(min);
	    slider.setMaximum(max);
	}                 
    } 


    public ActionListener zoomInActionListener() 
    {
	return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
		zoomIn();
            }
        };
    }

    public ActionListener zoomOutActionListener() 
    {
	return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
		zoomOut();
            }
        };
    }

    public Component getSlider()
    {
	return slider;
    }
}
