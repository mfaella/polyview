package polyview.view;

import com.jogamp.opengl.GL;
import static com.jogamp.opengl.GL.GL_COLOR_BUFFER_BIT;
import static com.jogamp.opengl.GL.GL_DEPTH_BUFFER_BIT;
import static com.jogamp.opengl.GL.GL_NICEST;
import com.jogamp.opengl.GL2;
import static com.jogamp.opengl.GL2ES1.GL_PERSPECTIVE_CORRECTION_HINT;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import static com.jogamp.opengl.fixedfunc.GLLightingFunc.GL_SMOOTH;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_MODELVIEW;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_PROJECTION;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.awt.TextRenderer;
import java.awt.Color;
import java.awt.Font;

import java.util.Iterator;
import java.util.List;
import polyview.model.Coordinate;
import polyview.model.PairDraw;
import polyview.model.PowerSet.PowerSetView;
import java.text.DecimalFormat;
 
/**
 * Componente canvas(per la visualizzazione del poliedro) che è aggiunto nel Container top-level(MainFrame),
 * questo gestisce gli eventi OpenGL per il rendering grafico del poliedro
 */

@SuppressWarnings("serial")
public class CanvasPolyhedron extends GLCanvas{
    
    //Fornisce l'accesso alla libreria di utilità OpenGL (GLU). 
    private GLU utilityOpenGl;  
    //Lista dei Poliedri da visualizzare nel panel
    private List<PowerSetView> setPolyhedronNNC;
    //Coppia di label degli assi cartesiani
    private PairDraw<String> variableAxes;
    
    //Valori estremi (variabili in base al range di visualizzazione delle varibili)del viewing volume
    private float xMin;
    private float xMax;
    private float yMin;
    private float yMax;
    
    //Valori estremi (variabili calcolate in base al range di visualizzazione delle varibili e dalle dimensioni del range)dipendenti dalla dimensione del frame
    private float xFmin;
    private float xFmax;
    private float yFmin;
    private float yFmax;
    
    private float moveIncrementX;   //Move along axis X
    private float moveIncrementY;   //Move along axis Y
    private float zoom;   //Scale both axes X and Y 
    //Flag per vedere se è abilitata la funzione di mostrare le Patches(cioè visualizzare il PowerSet evidenziandone i Poliedri)
    private boolean showPatches;
    private DecimalFormat myFormatter = new DecimalFormat("##0.##");
    
    /** Costruttore per impostare la GUI per questo componente */
    public CanvasPolyhedron() {
        
      //Creo il controller
      DrawPoliedro temp=new DrawPoliedro(this);
      //Aggiungo il GLEventListener
      this.addGLEventListener(temp);
      //Inizializzo la coppia dei label degli assi
      variableAxes=new PairDraw<>();
      
      showPatches=false;
    }
    
    public void setVariableAxes(String variable,float minValue,float maxValue){
        variableAxes.add(variable);
        //Imposto il range della variabile appena settata
        if(variableAxes.getFirst().equals(variable)){
            if(!Float.isInfinite(minValue))
                xMin=minValue;
            else
                xMin=-50;
            if(!Float.isInfinite(maxValue))
                xMax=maxValue;
            else
                xMax=50;
        }else{
            if(!Float.isInfinite(minValue))
                yMin=minValue;
            else
                yMin=-50;
            if(!Float.isInfinite(maxValue))
                yMax=maxValue;
            else
                yMax=50;
        }
    }
    
    public void unSetVariableAxes(String variable){
        variableAxes.remove(variable);
    }
    //Resetto gli assi 
    public void resetAxes(){
        variableAxes.resetFirst();
        variableAxes.resetSecond();
    }
    public class DrawPoliedro implements GLEventListener{
        private final TextRenderer textRenderer;
        
        private final int marginGraph=60;
        private final CanvasPolyhedron canvas;
        
        
       /** ------ Implementazione metodi dichiarati nell'interfaccia di GLEventListener ------.
         * @param canvas */
        public DrawPoliedro(CanvasPolyhedron canvas){
            //Allocate textRenderer with the chosen font
            textRenderer = new TextRenderer(new Font("SansSerif", Font.PLAIN, 10),true,false);  
            this.canvas=canvas;
            moveIncrementX=-0.0f;
            moveIncrementY=0.0f;
            zoom=2.5f;
        }
        
        @Override
        public void init(GLAutoDrawable drawable) {
      
            GL2 gl = drawable.getGL().getGL2();   // fornisce il contesto OpenGL graphics
            utilityOpenGl = new GLU();                         // get GL Utilities
           
            gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // imposto il colore di background (clear)
            gl.glClearDepth(1.0f);      // set clear depth value to farthest

            gl.glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
            gl.glShadeModel(GL_SMOOTH); // blends colors nicely, and smoothes out lighting
        }
 
       /**
        * Richiamato dal handler per un evento di  window re-size . Chiamato anche quando the drawable è
        * reso visibile la prima volta.
        */
        @Override
        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
             GL2 gl = drawable.getGL().getGL2();  // get the OpenGL 2 graphics context
             
            //Setto la view port (display area)in modo da coprire l'intera finesta
            gl.glViewport(0,0, width, height);
            
            //prevent divide by zero
            if (height == 0) 
               height = 1;  
            
            //Aspect radio calcolato in base alle dimensioni della finestra
            float aspectRadio = (float)width / height;
            
            // Imposto come matrice corrente la GL_PROJECTION (in questo modo le prox modifiche effettuate su matrice saranno su di essa)
            gl.glMatrixMode(GL_PROJECTION);  
            gl.glLoadIdentity();// reset projection matrix

            //Imposto le coordinate in base al range dei valori forniti dalle variabili da disegnare
            if(width <= height) {
                xFmin=-150;
                xFmax=150;
                yFmin=-150/aspectRadio;
                yFmax=150/aspectRadio;
                utilityOpenGl.gluOrtho2D(xFmin,xFmax,yFmin,yFmax);  // aspect <= 1
            }
            else {
                xFmin=-150* aspectRadio;
                xFmax=150* aspectRadio;
                yFmin=-150;
                yFmax=150;
                utilityOpenGl.gluOrtho2D(xFmin, xFmax, yFmin,yFmax);  // aspect > 1
            }
            
           // Imposto come matrice corrente la GL_MODELVIEW (in questo modo le prox modifiche effettuate su matrice saranno su di essa)
           gl.glMatrixMode(GL_MODELVIEW);
           gl.glLoadIdentity(); // reset ModelView matrix
        }
        
       /**
        * Richiamato da animator per eseguire il rendering.
         * @param drawable
        */
        @Override
        public void display(GLAutoDrawable drawable) {
            GL2 gl = drawable.getGL().getGL2(); // get the OpenGL 2 graphics context
            
            gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear color and depth buffers
            gl.glLoadIdentity();  // reset the model-view matrix
            drawGraph(drawable);

            //Imposto il box del plot
            gl.glScissor(marginGraph, marginGraph,drawable.getSurfaceWidth()-marginGraph*2, drawable.getSurfaceHeight()-marginGraph*2);
            gl.glEnable(GL2.GL_SCISSOR_TEST);
            
            //Variabili usate nei cicli
            Coordinate tempPoint1; 
          
            //Traslo e/o scalo gli oggetti in base alla vista di default ed quella impostata dall'utente 
            gl.glTranslatef(moveIncrementX*zoom + xFmin - xMin*zoom,
			    moveIncrementY*zoom + yFmin - yMin*zoom, 0);
            gl.glScalef(zoom, zoom, 0);
            Color colorPoly;
            if(setPolyhedronNNC!=null){
                for(PowerSetView powerSet:setPolyhedronNNC){
                    //Mi procuro il colore assegnato al poliedro
                    colorPoly=powerSet.getColorPowerSet();
                    gl.glColor4f(colorPoly.getRed(),colorPoly.getGreen(),colorPoly.getBlue(),0.75f);
                    //Scorro la lista dei poliedriNNC
                    for(List<Coordinate> listPointDraw:powerSet.getListPolyhedrons()){
                        if(listPointDraw.isEmpty())
                            throw new RuntimeException("Lista coordinate nulle");
                        
                        Iterator<Coordinate> iteratoreCoordinate = listPointDraw.iterator();
                        if(iteratoreCoordinate==null)
                            throw new RuntimeException("Iteratore coordinate nullo");
                        
                        //Controllo se ci sono punti
                        if(iteratoreCoordinate.hasNext()){
                            //Setto le primitive in base al numero di punti
                            if(listPointDraw.size()>2)
                                drawPolyhedron(drawable,listPointDraw);
                        }
                        else{
                            //Prelevo l'unica coordinata e l'aggiungo alla primitiva
                            if(iteratoreCoordinate.hasNext()){
                                tempPoint1=iteratoreCoordinate.next();//Prelevo la coordinata
                                gl.glBegin(GL2.GL_POINTS);
                                gl.glVertex2d(tempPoint1.getCoordinateAxis1(),tempPoint1.getCoordinateAxis2());
                                gl.glEnd();
                            }
                        }
                    }
                    
                     //Dopo il primo powerSet abilito la funzione di blending per la trasparenza
                    gl.glEnable(GL2.GL_BLEND);
                    gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE_MINUS_SRC_ALPHA);
                }
                gl.glDisable(GL2.GL_BLEND);
                
                if(showPatches==true){
                    for(PowerSetView powerSet:setPolyhedronNNC){
                        //Scorro la lista dei poliedriNNC
                        for(List<Coordinate> listPointDraw:powerSet.getListPolyhedrons()){
                            if(listPointDraw.isEmpty())
                                throw new RuntimeException("Lista coordinate nulle");
                            drawEdgeBoundary(drawable,listPointDraw);
                        }
                    }
                }
                
            }
            gl.glDisable(GL2.GL_SCISSOR_TEST);
            gl.glDisable(GL2.GL_BLEND);
            
        }       
        
        public void drawGraph(GLAutoDrawable drawable) {
            GL2 gl = drawable.getGL().getGL2(); // get the OpenGL 2 graphics context

            //Pulisco il buffer dei colori
            gl.glClear(GL.GL_COLOR_BUFFER_BIT);
            gl.glPolygonMode(GL2.GL_FRONT_AND_BACK,GL2.GL_FILL);
            //Imposto il colore ed lo spessore della linea del grafico
            gl.glColor3f(0.0f, 0.2f, 0.4f);
            gl.glLineWidth(2.0f);
            
            //Valore minimo e massimo del range dell'asse orizzontale e verticale
            float minValueAxes1; 
            float minValueAxes2;
            
            float width=drawable.getSurfaceWidth()-marginGraph*2;
            float height= drawable.getSurfaceHeight()-marginGraph*2;
            
            float offsetX=(2.0f*marginGraph+(width-drawable.getSurfaceWidth()))/drawable.getSurfaceWidth();
            float offsetY=(2.0f*marginGraph+(height-drawable.getSurfaceHeight()))/drawable.getSurfaceHeight();
            
            float scaleX=width/drawable.getSurfaceWidth();
            float scaleY=height/drawable.getSurfaceHeight();
            
	    // independent of zoom
	    final int numberOfTicks = 20;
	                
	    // System.out.println("incrX: " + moveIncrementX + ", scaleX: " + scaleX + ", xMin:" + xMin + 
	    //	       ", xFmin:" + xFmin + ", zoom: " + zoom);
	    
            //Traslo e/o scalo gli oggetti in base alla vista di default ed quella impostata dall'utente 
            gl.glScalef(scaleX,scaleY,0);
            gl.glTranslatef(offsetX, offsetY, 0);
            
            //Inizio il draw del grafico
            gl.glBegin(GL2.GL_LINES);
            
            //Draw l'asse verticale
            gl.glVertex2d(xFmin,yFmin);
            gl.glVertex2d(xFmin,yFmax);
            //Draw l'asse orizzontale
            gl.glVertex2d(xFmin,yFmin);
            gl.glVertex2d(xFmax,yFmin);
            
	    float tickStepX = (xFmax-xFmin)/numberOfTicks, 
	          tickStepY = (yFmax-yFmin)/numberOfTicks;

            // Ticks for y
            for(float i=yFmin; i<yFmax; i+=tickStepY) {
                gl.glVertex2d(xFmin+5, i);
                gl.glVertex2d(xFmin-5, i);
            }
 
            // Ticks for x
            for(float i=xFmin; i<xFmax; i+=tickStepX) {
                gl.glVertex2d(i, yFmin+5);
                gl.glVertex2d(i, yFmin-5);
            }
            
            //Chiudo la primitiva
            gl.glEnd();
            
            //Inizio rendering font
            textRenderer.begin3DRendering();
            
            //Draw passo per l'asse orizzontale
            if (variableAxes.getFirst()!=null) {
                for (float i=xFmin; i<xFmax; i+= 2*tickStepX) {
		    float label = (i - xFmin)/zoom + xMin - moveIncrementX;
		    String labelText = myFormatter.format( label );
		    textRenderer.draw3D(labelText, i-4, yFmin-15, 0, 0.5f);
                }
            }
            
            //Draw passo per l'asse verticale
            if (variableAxes.getSecond()!=null) {
                for(float i=yFmin; i<yFmax; i+= tickStepY)  {
		    float label = (i - yFmin)/zoom + yMin - moveIncrementY;
		    String labelText = myFormatter.format( label );
                    textRenderer.draw3D(labelText, xFmin-23, i-2, 0, 0.5f);
                 }
            }
            
            //Draw label assi 
            //Label per l'asse orizzontale
            if(variableAxes.getFirst()!=null)
                textRenderer.draw3D(variableAxes.getFirst(), xFmax+10, yFmin, 0, 0.7f);
            
            //Label per l'asse verticale
            if(variableAxes.getSecond()!=null)
                textRenderer.draw3D(variableAxes.getSecond(), xFmin, yFmax+10, 0, 0.7f);
            
            textRenderer.end3DRendering();
        }   
        
        /**
        * Richiamato prima che il contesto OpenGl è distrutto the OpenGL.Risorsa d'uscita come ad esempio il buffer
         * @param drawable
        */
        @Override
        public void dispose(GLAutoDrawable drawable) {}
        
        /**Vedo i vari casi:.
               *vertex proiettato da un ray 
               *un vertex proiettato da una linea
               *un vertex di chiusura
               *un vertex normale
        */
            
        public void drawEdgeBoundary(GLAutoDrawable drawable, List<Coordinate> listPointDraw){
            GL2 gl = drawable.getGL().getGL2(); // get the OpenGL 2 graphics context
            
            Coordinate tempPoint;
            //Mi procuro l'iteratore delle loro coordinate
            Iterator<Coordinate> iteratoreCoordinate = listPointDraw.iterator();

            gl.glPolygonMode(GL2.GL_FRONT_AND_BACK,GL2.GL_LINE);
             
            gl.glLineWidth(2.0f);
            gl.glColor3f(1.00f,1.00f,1.00f);
            //Marco i contorni della zona chiusa
            gl.glBegin(GL2.GL_POLYGON); 
            //Scorro le coordinate dei punti ed le aggiungo alla primitiva
            while(iteratoreCoordinate.hasNext()){
                 tempPoint=iteratoreCoordinate.next();//Prelevo la coordinata
                 gl.glVertex2d(tempPoint.getCoordinateAxis1(),tempPoint.getCoordinateAxis2());
            }
            
            gl.glEnd();
        }
        
        
        public void drawPolyhedron(GLAutoDrawable drawable, List<Coordinate> listPointDraw){
            GL2 gl = drawable.getGL().getGL2(); // get the OpenGL 2 graphics context
            Coordinate tempPoint;
            
            //Mi procuro l'iteratore delle loro coordinate
            Iterator<Coordinate> iteratoreCoordinate = listPointDraw.iterator();
            
            gl.glPolygonMode(GL2.GL_FRONT_AND_BACK,GL2.GL_FILL);
            
            //Inizializzo la primitiva
            gl.glBegin(GL2.GL_POLYGON);  
            
            //Scorro le coordinate dei punti ed le aggiungo alla primitiva
            while(iteratoreCoordinate.hasNext()){
                 tempPoint=iteratoreCoordinate.next();//Prelevo la coordinata
                 gl.glVertex2d(tempPoint.getCoordinateAxis1(),tempPoint.getCoordinateAxis2());
            }
            gl.glEnd();
                    
        }
    }
    
    public void setZoom(float zoom){
        this.zoom=zoom;
        //Esegue il renderer del canvas
        display();
    }
    
    public void incrementX(){
        if(variableAxes.fullVariable()){
            moveIncrementX +=10;
            //Esegue il renderer del canvas
            display();
        }
    }
    public void decrementX(){
        if(variableAxes.fullVariable()){
            moveIncrementX -=10;
            //Esegue il renderer del canvas
            display();
        }
    }
    public void incrementY(){
        if(variableAxes.fullVariable()){
            moveIncrementY+=10;
            //Esegue il renderer del canvas
            display();
        }
    }
    public void decrementY(){
        if(variableAxes.fullVariable()){
            moveIncrementY -=10;
            //Esegue il renderer del canvas
            display();
        }
    }
    public void setListPolyhedronNNC(List<PowerSetView> ListPolyhedronNNC) {
        this.setPolyhedronNNC = ListPolyhedronNNC;
    }
    public void setViewPortGraph(float xMin,float xMax,float yMin,float yMax){
        this.xMin=xMin;
        this.xMax=xMax;
        this.yMin=yMin;
        this.yMax=yMax;
    }
    
    
    public boolean isShowPatches() {
        return showPatches;
    }

    public void setShowPatches(boolean showPatches) {
        this.showPatches = showPatches;
        this.display();
    }
    
}

 
   
