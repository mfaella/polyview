
package polyview.model;

/**
 *
 * @author nello
 * @param <T>
 */
public class PairDraw<T> extends Pair<T> {

    //Aggiungo l'elemento alla coppia 
    public void add(T element){
        //Se l'elemento è null lancio un eccezione
        if(element==null)
            throw new RuntimeException("Non posso avere due elementi uguali o essere null");
        //se è già presente non faccio nulla
        if(!element.equals(this.getFirst())&&!element.equals(this.getSecond())){
            
             //Se è pieno lancio un eccezione
            if(fullVariable())
                throw new RuntimeException("Posti esauriti");
            //Vedo quale posto è libero
            if(this.getFirst()==null)
                this.addFirst(element);
            else
                this.addSecond(element);
        }
    }
    
    //Rimuove l'elemento dalla coppia se non è presente non fa nulla
    public void remove(T element){
        if(element==null)
           throw new RuntimeException("Element null");
        
        if(element.equals(this.getFirst()))
            this.resetFirst();
        else if(element.equals(this.getSecond()))
            this.resetSecond();
    }

    //Se le due variabili sono inizializzate ritorno true
    public boolean fullVariable(){
        if(this.getFirst()!=null && this.getSecond()!=null)
            return true;
        else
            return false;
    }
    public boolean contains(T element){
        return element.equals(this.getFirst())||element.equals(this.getSecond());
    }
    public boolean isEmpty(){
        return getFirst()==null && getSecond()==null;
    }
}
