
package polyview.model;

/**
 *
 * @author nello
 */

public class Pair<T> {
    private T first;
    private T second;
    
    public T getFirst(){
        return first;
    }
    public T getSecond(){
        return second;
    }
    public Pair(){
        
    }
    public Pair(T first,T second){
        this.first=first;
        this.second=second;
    }
    
    //Aggiungo l'elemento alla coppia 
    public void addFirst(T element){
        //Se l'elemento è null lancio un eccezione
        if(element==null)
            throw new RuntimeException("Non posso avere elementi null");
       
        this.first=element;
  
    }
    public void addSecond(T element){
        //Se l'elemento è null lancio un eccezione
         if(element==null)
            throw new RuntimeException("Non posso avere elementi null");
         
         this.second=element;
    }
    public void resetFirst(){
        first=null;
    }
    public void resetSecond(){
        second=null;
    }
}
