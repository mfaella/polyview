
package polyview.model;

/**
 *
 * @author nello
 */
public class VariableValue {
    private String variable; 
    private double value; 
    
    public VariableValue(String variable,double value){
        this.variable=variable;
        this.value=value;
    }
    public String getVariable(){
         return variable;   
    }
    public double getValue(){
         return value;   
    }
}
