
package polyview.model;

import java.awt.Color;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import parma_polyhedra_library.By_Reference;
import parma_polyhedra_library.Coefficient;
import parma_polyhedra_library.Constraint;
import parma_polyhedra_library.Constraint_System;
import parma_polyhedra_library.NNC_Polyhedron;
import parma_polyhedra_library.Parma_Polyhedra_Library;
import parma_polyhedra_library.Pointset_Powerset_NNC_Polyhedron;
import parma_polyhedra_library.Relation_Symbol;
import parma_polyhedra_library.Variable;
// import parma_polyhedra_library.Double_Box;
import parma_polyhedra_library.Linear_Expression;
import parma_polyhedra_library.Linear_Expression_Coefficient;
import parma_polyhedra_library.Linear_Expression_Times;
import parma_polyhedra_library.Pointset_Powerset_NNC_Polyhedron_Iterator;
import parma_polyhedra_library.Variable_Stringifier;
import parma_polyhedra_library.Variables_Set;
import polyview.model.PowerSet.Polyhedron;

import polyview.model.PowerSet.PowerSetView;

/**
 *
 * @author nello
 */

/*Classe che deve gestire le operazioni con PPL ed ha un proprio stato che e' osservato dalla viste*/

public class PPLModel{
    
    //Mappe che gestiscono le corrispondente delle variabili(mappe associative)
    private final Map<Long,String> mappaAssociativaIntegerKey;
    private final Map<String,Long> mappaAssociativaStringKey;
    
    //Campo che gestisce gli estremi dei valori delle varie variabili del modello     
    private final Map<Long,Pair<Float>> minMaxVariable;
    
    //Campo che gestisce il numero delle variabili create all'interno del modello
    private long variableIndex;
    
    //Mappa che gestisce i simboli Relazionali 
    protected static final Map<String,Relation_Symbol> relationSymbols=new HashMap<>();
    
    // Lista di PowerSet di NNC_Poliedri
    private final List<PowerSet> listPowerSet;
    
    //Array dei colori disponibili
    private final Color[] listColor={Color.cyan,Color.green,Color.magenta,Color.red};
    //Indice colori
    private int colorIndex=0;
   
    //Costruttore privato
    private PPLModel(){
        //Inizializzo le varie strutture
        mappaAssociativaIntegerKey=new HashMap();
        mappaAssociativaStringKey=new HashMap();
        listPowerSet=new ArrayList<>();
        minMaxVariable=new HashMap<>();
       
    }

    //Metodo statico che crea ed inizializza il modello con i PowerSet contenuti all'interno del file dato in input
    public static PPLModel inizializePoliedri(File filePoly) throws FileNotFoundException{
        //Inizializzo la libreria ppl 
        Parma_Polyhedra_Library.initialize_library();
        
        //Creo una nuova istanza del modello da inizializzare con i dati letti dal file 
        PPLModel modello=new PPLModel();
        //Inizializzo le relazioni
        loadRelazioni();
        //Carico i dati dal file inizializzando il modello(gli passo il modello da inizializzare ed il file di testo)
        loadPoliedri(filePoly,modello);
        
        //Se non viene caricato nessun poliedro
        if(modello.isEmptyPolyhedrons())
            throw new IllegalArgumentException("ErroreConstraintFile: I vincoli non producono nessun poliedro");
        
        //Rilascio le risorse della libreria
        Parma_Polyhedra_Library.finalize_library();
        
        //Ritorno il modello inizializzato con i dati del file
        return modello;
    }

    //Metodo statico(overload di quello precedente) che aggiunge i powerSet contenuti nel file di input al modello passato
    public static void inizializePoliedri(File filePoly,PPLModel modello) throws FileNotFoundException{
        //Inizializzo la libreria ppl 
        Parma_Polyhedra_Library.initialize_library();
        
        //Carico i dati dal file inizializzando il modello(gli passo il modello da inizializzare ed il file di testo)
        loadPoliedri(filePoly,modello);
        
        //Se non viene caricato nessun poliedro
        if(modello.isEmptyPolyhedrons())
            throw new IllegalArgumentException("ErroreConstraintFile: I vincoli non producono nessun poliedro");
        
        //Rilascio le risorse della libreria
        Parma_Polyhedra_Library.finalize_library();
    }
    
    //Aggiunge il powerSet passato in input con il relativo nome,al modello(this)
    public void addPowerSetNNCPoliedri(Pointset_Powerset_NNC_Polyhedron powerSetNNCPoly,String nomePowerSet){
        //Controllo che i parametri passati in input non siano nulli
        if(powerSetNNCPoly==null || nomePowerSet==null)
            throw new RuntimeException("Poliedro vuoto");
       
        //Nel caso in cui il powerSet e' senza nome lo aggiungo di default in base al numero di powerSet inseriti
        if(nomePowerSet.isEmpty())
            nomePowerSet="PowerSet"+(listPowerSet.size()+1);
       
        //Aggiungo il PowerSet alla lista il powerSet appena creato
        listPowerSet.add(new PowerSet(powerSetNNCPoly,nomePowerSet,listColor[colorIndex],this));
        
        //Aggiorno l'indice dei colori
        colorIndex++;
        //Nel caso in cui ho saturato i colori,resetto l'indice a 0
        if(colorIndex>=listColor.length)
            colorIndex=0;
    }
    
    //Metodo che controlla se il moedello non contiene nessun poliedro
    public boolean isEmptyPolyhedrons(){
        boolean temp=false;
        if(listPowerSet!=null)
            temp=listPowerSet.isEmpty();
        return temp;
    }
    
    //Metodo per inizializzare la mappa che tiene in corrispondenza le stringhe contenente i simboli di relazioni con le costanti di relazione di ppl
    private static void loadRelazioni(){
        //Se e' stato gia' inizializzato non lo faccio
        if(relationSymbols.isEmpty()){
            relationSymbols.put("==",Relation_Symbol.EQUAL);
            relationSymbols.put(">=",Relation_Symbol.GREATER_OR_EQUAL);
            relationSymbols.put(">",Relation_Symbol.GREATER_THAN);
            relationSymbols.put("<=",Relation_Symbol.LESS_OR_EQUAL);
            relationSymbols.put("<",Relation_Symbol.LESS_THAN);
            relationSymbols.put("!=",Relation_Symbol.NOT_EQUAL);
        }
    }
    
    //Metodo che prepara le coordinate dei punti,raggi,linee,punti di chiusura sul modello passato che poi verrano utilizzate dalle view corrispondente
    public boolean drawPolyhedrons(ModelFrame modelFrame){
        //Inizializzo la libreria
        Parma_Polyhedra_Library.initialize_library();
        //Lista che conterra la lista di coordinate dei powerSet cioe' List<PowerSet>
        List<List<Coordinate>> tempPowerSets;
        //Variabile temporanea per il powerSet
        PowerSet tempPowerSet;
        //Variabile booleana che sara' restituita come risultato
        boolean ok=false;
        
        //Nel caso la lista sia vuota fare una eccezione verificata cosi da poter essere gestita dal main frame ed visualizzare una finestra di errore
        if(listPowerSet.isEmpty())
            throw new RuntimeException("Nessun poliedro caricato");
        //Vedo se il modello può procedere al draw 
        if(modelFrame.isDraw()){
            //Mi procuro la copia PowerSet semplificato del modello della vista
            List<PowerSetView> powerSetView = modelFrame.getPowerSetView();
            //Scorro la lista dei PowerSetNNCPolyhedron ed setto i vari polyhedron in base alle scelte dell'utente
            for(int i=0;i<listPowerSet.size();i++){
                //Mi procuro il powerSet del modello principale
                tempPowerSet=listPowerSet.get(i);
                //Mi procuro le coordinate settate in base alle scelte del canvas del powerSet
                tempPowerSets=tempPowerSet.setPowerSet(modelFrame);
                //Se il poliedro non e' nullo metto ok=true,cioe' metto il flag che mi indica che c'e' almeno un powerSet disponibile
                if(!tempPowerSets.isEmpty())
                    ok=true;

                //Mi salvo le coordinate settate nel modello del canvas
                powerSetView.get(i).setListPowerSet(tempPowerSets);
            }
        }else
            ok=false; //Altrimenti metto direttamente a false
        
        //Setto lo stato in base al numero di poliedri trovati
        modelFrame.setState(ok);
        
        //Dico al modelloFrame di poter notificare il canvas
        modelFrame.notifyObserver();
        
        //Chiudo la libreria
        Parma_Polyhedra_Library.finalize_library();
        //Ritorno il flag che indica se c'e' almeno un poliedro
        return ok;

    }
    
    public void stampaPoly(NNC_Polyhedron poly,String message){
        System.out.println(message);
        System.out.println("Polyhedron generators "+poly.generators());
        System.out.println("Polyhedron constraint"+poly.constraints());
    }  
    
    //Metodo get che ritorna la lista dei powerSet del modello(this) 
    public List<PowerSet> getPolyhedrons(){
        return listPowerSet;
    }
    //Metodo che crea una nuova variabile nel modello
    public Variable createVariable(String Variable){
        Variable temp;
        //Se la stringa(quindi la variabile)non e' presente la creo inserendo l'associazione (indice,string)
        //Creo un associazione in entrambi i lati ed creo l'oggetto Variable in base all'indice
        if(!mappaAssociativaIntegerKey.containsValue(Variable)){
            mappaAssociativaIntegerKey.put(variableIndex,Variable);
            mappaAssociativaStringKey.put(Variable,variableIndex);
            temp=new Variable(variableIndex);
            variableIndex++;
        }
        else
            temp=new Variable(mappaAssociativaStringKey.get(Variable)); //Altrimenti creo semplicemente l'oggetto Variable con l'indice della variabile senza creare le associazioni
        
        return temp;
    }
    //Metodo che restituisce l'indice della variabile string
    public long getIndiceVariable(String variabile){
        return mappaAssociativaStringKey.get(variabile);
    }   
    //Metodo che restituisce la stringa corrispondente all'indice
    public String getStringVariable(long indice){
        return mappaAssociativaIntegerKey.get(indice);
    } 
    //Metodo che restituice il numero delle variabili(quindi lo spazio dimensionale del modello)
    public long spaceDimension(){
        return variableIndex;
    }
    
    //Ritorna un set di stringhe contenente tutte le variabili del modello
    public Set<String> getAllStringVariable(){
        return mappaAssociativaStringKey.keySet();
    }
    
    //Metodo Factory che crea un nuovo modello da associare al singolo Frame
    public ModelFrame createModelFrame(){
        return new ModelFrame();
    }
    
    public class ModelFrame extends Observable{
        
        //Campi che gestiscono i controlli delle variabili da proiettare,sezionare,disegnare
        private PairDraw<Long> variableDraw;
        private Set<Long> variableProject;
        private Map<Long,Integer> valueVariable;
        private final  List<PowerSetView> listPowerSetView;
        private boolean state;
        
        //Costruttore ModelFrame
        private ModelFrame(){
            //Inizializzo i vari campi
            variableDraw=new PairDraw<>();
            variableProject=new HashSet<>();
            valueVariable=new HashMap<>();
            listPowerSetView=new ArrayList<>();
            state=true;
            
            //Inizializzo la lista dei powerSet per la view
            for(PowerSet temp:listPowerSet)
                listPowerSetView.add(temp.createPowerSetFrame());
          
        }
        //Metodo che aggiorna la lista dei powerSet se sono stati aggiunti dei nuovi al modello principale
        public void updateListPowerSet(){
            //Mi procuro il numero di powerSet
            int sizePowerSet=listPowerSet.size();
            PowerSet temp;
            //Se ci sono nuovi PowerSet li aggiungo alla lista
            if(sizePowerSet>listPowerSetView.size()){
                //Aggiorno la lista dei powerSet per la view
                for(int i=listPowerSetView.size();i<sizePowerSet;i++){
                    //Mi procuro il new powerSet 
                    temp=listPowerSet.get(i);
                    //Creo ed aggiungo il PowerSetView
                    listPowerSetView.add(temp.createPowerSetFrame());
                }
            }
        }
        public void resetModelPowerSet(){
            //Inizializzo i vari campi
            variableDraw=new PairDraw<>();
            variableProject=new HashSet<>();
            valueVariable=new HashMap<>();
        }
        //Metodo che setta la variabile passata in input in Draw
        public void setVariableDraw(String variable){
            if(!mappaAssociativaStringKey.containsKey(variable))
                throw new RuntimeException("Variabile non esistente");
           
            //Se la coppia e' completa lancia un eccezione se e' gia' presente non succede nulla
            variableDraw.add(mappaAssociativaStringKey.get(variable));
        }
        //Metodo che toglie la variabile passata in input in Draw(Se non e' presente in draw non fa nulla)
        public void unSetVariableDraw(String variable){
            if(!mappaAssociativaStringKey.containsKey(variable))
                throw new RuntimeException("Variabile non esistente");
            //Se non c'e' non fa nulla
            variableDraw.remove(mappaAssociativaStringKey.get(variable));
        }   
        //Metodo get che restituisce la coppia delle variabili in draw
        public PairDraw<Long> getVariablesDraw(){
            return variableDraw;
        }    
        //Metodo che settta la variabile passata in input in project, se e' gia' presente ritorna false,altrimenti true
        public boolean setVariableProject(String variable){
            if(!mappaAssociativaStringKey.containsKey(variable))
                throw new RuntimeException("Variabile non esistente");
        
            return variableProject.add(mappaAssociativaStringKey.get(variable));
        }
        //Metodo che toglie la variabile passata in input da project,ritorna true se e' presente,false altrimenti
        public boolean unSetVariableProject(String variable){
            if(!mappaAssociativaStringKey.containsKey(variable))
                throw new RuntimeException("Variabile non esistente");
            
            return variableProject.remove(mappaAssociativaStringKey.get(variable));
        }
        //Metodo get che ritorna il set di variabili messe in project
        public Set<Long> getVariablesProject(){
            return variableProject;
        } 
        //Metodo che associa il valore con la variabile passati come input
        public void setVariableValue(String variable,Integer value){
            if(!mappaAssociativaStringKey.containsKey(variable))
                throw new RuntimeException("Variabile non esistente");
            
            valueVariable.put(mappaAssociativaStringKey.get(variable), value);
        }
        //Metodo che toglie il valore alla variabile passata in input
        public void unSetVariableValue(String variable){
            if(!mappaAssociativaStringKey.containsKey(variable))
                throw new RuntimeException("Variabile non esistente");
            
            valueVariable.remove(mappaAssociativaStringKey.get(variable));
        }
        //Metodo che ritorna una mappa stringa valore
        public Map<Long,Integer> getVariablesValue(){
            return valueVariable;
        }
        
        //Metodo che ritorna un set di stringhe contenente tutte le variabili del modello
        public Set<String> getAllStringVariable(){
            return mappaAssociativaStringKey.keySet();
        }
        //Metodo che notifica alle viste i cambiamenti
        private void notifyObserver(){
            //Dico che lo stato dell'oggetto e' cambiato ed lo notifico agli osservatore del modello
            this.setChanged();
            this.notifyObservers();
        }
        //Metodo che ritornal la lista di PowerSetView associata al modello
        public List<PowerSetView> getPowerSetView(){
            return listPowerSetView;
        }
        
        public boolean getState(){
            return state;
        }
        
        private void setState(boolean state){
            this.state=state;
        }
        
        //Può disegnare se ho 2 variabili a draw ed le restanti proiettate o sezionate
        public boolean isDraw(){
            return variableDraw.fullVariable() && 
		(valueVariable.size()+this.variableProject.size())>=(mappaAssociativaStringKey.size()-2);
        }
        
        //Metodo che indica se una variabile e' in draw(true) oppure no(false)
        public boolean isVariableDraw(long variable){
            return getVariablesDraw().contains(variable);
        }
        //Metodo che inizia la preparazione per salvare l'instantanea attuale su un file di testo
        public void savePowerSet(Path pathSave) throws IOException{
            //Variabile che conterra il powerSet sezionato ed ridotto ad due dimensioni
            Pointset_Powerset_NNC_Polyhedron powerSet1;
            //Ordine delle variabili per la corrispondenza degli indici
            String variableDraw1,variableDraw2;
            //Vedo quale variabile in Draw ha indice inferiore,per stabilire la corrispondenza temporanea per chi avra' indice 0 e chi 1(2-D)
            if(getVariablesDraw().getFirst()>getVariablesDraw().getSecond()){
                variableDraw1 =getStringVariable(getVariablesDraw().getSecond());
                variableDraw2 =getStringVariable(getVariablesDraw().getFirst());
            }else{
                variableDraw1 =getStringVariable(getVariablesDraw().getFirst());
                variableDraw2 =getStringVariable(getVariablesDraw().getSecond());
            }
            
            //Inizializzo la libreria
            Parma_Polyhedra_Library.initialize_library();
            //Apro il file dove salvare i dati in scrittura 
            try (BufferedWriter writer = Files.newBufferedWriter(pathSave, StandardCharsets.UTF_8)) {
                //Ad ogni powerSet aggiungo i vincoli di sezionamento ed proezione ed lo riduco a 2-D ed lo salvo sul file
                for(PowerSet tempPowerSet:listPowerSet){
                    //Applico il sezionamento
                    powerSet1=addVariableValue(tempPowerSet);
                    //Se ha avuto buon esito allora lo salvo sul file
                    if(powerSet1!=null) 
                        savePowerSets(variableDraw1,variableDraw2,tempPowerSet.getNomePowerSet(),powerSet1,writer);
                }
             
            } catch (IOException x) {
                System.err.format("IOException: %s%n", x);
            }
            //Chiudo la libreria
            Parma_Polyhedra_Library.finalize_library();
        }
        //Metodo d'appoggio ad savePowerSet
        public Pointset_Powerset_NNC_Polyhedron addVariableValue(PowerSet powerSet){
            //Espressioni lineari della parte sx e dx
            Linear_Expression expSx;
            Linear_Expression expDx;
            //Inizializzo il sistema di constraint
            Constraint_System vincolo;
            vincolo=new Constraint_System();
            
            //Insieme che costruisco iterativamente per tenere traccia della variabili sezionate
            Variables_Set setVariable=new Variables_Set();
            for(Long temp:getVariablesValue().keySet()){
                //Creo ed aggiungo il vincolo al sistema per sezionare la variabile corrente
                expSx=new Linear_Expression_Times(new Coefficient(1l),createVariable(getStringVariable(temp)));
                expDx=new Linear_Expression_Coefficient(new Coefficient(getVariablesValue().get(temp)));
                vincolo.add(new Constraint(expSx,Relation_Symbol.EQUAL,expDx));
                //Mi salvo la variabile sezionata nell'insieme
                setVariable.add(new Variable(temp));
            }
            //Mi creo una copia del powerSet originale
            Pointset_Powerset_NNC_Polyhedron powerSet1=new Pointset_Powerset_NNC_Polyhedron(powerSet.getPointsetPowerSet());
            
            //Controllare se questa operazione va bene ad occhi sembra di si
            if(powerSet1.space_dimension()<spaceDimension())
                powerSet1.add_space_dimensions_and_embed(spaceDimension()-powerSet1.space_dimension());
           
            //Aggiungo i nuovi vincoli alla copia del powerSet originale
            powerSet1.add_constraints(vincolo);
            
            if(!powerSet1.is_empty()){
                //Aggiungo le altre variabili all'insieme
                for(Long temp:getVariablesProject())
                    setVariable.add(new Variable(temp));
            
                //Rimuovo le variabili che non sono state destinate al draw
                powerSet1.remove_space_dimensions(setVariable);
            }
            else
                powerSet1=null;
        
            return powerSet1;
        }
    }
    

    //Imposto il range delle variabili associato ad un poliedro,ed aggiorno gli estremi globali della variable
    public Map<Long,Pair<Float>> newBoxValue(NNC_Polyhedron poliedro){
        //Mi creo i coefficienti
        Coefficient n=new Coefficient(1);
        Coefficient d=new Coefficient(0);
        //Classe per calcolare il range di valori
        // Marco: Double_Box boxPoliedro;
        //Classe per tenere traccia dei due valori dell'intervallo
        Pair<Float> pairExtreme;
        //Classe che tiene la corrispondenza della variabile ed il proprio range
        Map<Long,Pair<Float>> rangeVariable;
        //Classe che mi dice se una variabile e' limitata oppure no
        By_Reference<Boolean> closed;
        //Valore ritornato
        float ris;
        if(poliedro==null)
            throw new RuntimeException("Poliedro per il box vuoto");

        //Creo il Box per il poliedro
        // boxPoliedro=new Double_Box(poliedro);
        rangeVariable=new HashMap<>();
        //Analizzo ogni variabile 
        for(long temp:mappaAssociativaIntegerKey.keySet()){
            //Inizializzo il riferimento closed
            closed=new By_Reference<>(false);
            //Inizializzo la coppia
            pairExtreme=new Pair<>();
            //Calcolo il lower Bound
            // boxPoliedro.has_lower_bound(new Variable(temp),n,d, closed);
            if (closed.get())
                ris=(n.getBigInteger().divide(d.getBigInteger())).floatValue();
            else
                ris=Float.NEGATIVE_INFINITY;
            //Aggiungo il lowerBound
            pairExtreme.addFirst(ris);
            //Inizializzo il riferimento closed
            closed=new By_Reference<>(false);
            //Calcolo l'upper Bound
            // boxPoliedro.has_upper_bound(new Variable(temp),n,d, closed);
            if(closed.get())
                ris=(n.getBigInteger().divide(d.getBigInteger())).floatValue();
            else
                ris=Float.POSITIVE_INFINITY;
            //Aggiungo l'upperBound
            pairExtreme.addSecond(ris);
            
            //Vedo se ho gia' dei valori globali salvati per quella variabile
            if(minMaxVariable.containsKey(temp)){
                //Vedo se l'estremo inferiore globale salvato per quella variabile e' maggiore di quello nuovo
                if(minMaxVariable.get(temp).getFirst()>pairExtreme.getFirst())
                    minMaxVariable.get(temp).addFirst(pairExtreme.getFirst());
                //Vedo se l'estremo superiore globale salvato per quella variabile e' minore di quello nuovo
                if(minMaxVariable.get(temp).getSecond()<pairExtreme.getSecond())
                    minMaxVariable.get(temp).addSecond(pairExtreme.getSecond());
            }else
                minMaxVariable.put(temp,new Pair<>(pairExtreme.getFirst(),pairExtreme.getSecond()));  //caso in cui e' la prima volta che incontro questa variabile
            
            //Inserisco il range della variabile nella collezione da ritornare
            rangeVariable.put(temp,pairExtreme);
        }
            
        return rangeVariable;
    }

    public Pair<Float> getUpperLowerBoundVariable(String variable){
        return minMaxVariable.get(this.mappaAssociativaStringKey.get(variable));
    }
    
    //Ritorna true se la variabile con il corrispottivo valore rispetta almeno un vincolo di qualche poliedro
    public boolean isVariableBox(long variable,float value){
        //Mi procuro il nome della variabile
        String nomeVariabile=mappaAssociativaIntegerKey.get(variable);
        //Variabile d'appoggio per upperLowerBound
        Pair<Float> upperLowerBoundVariable;
        //Risultato in output
        boolean ris=false;
        //Se il nome della variabile e' null,vuol dire che non ha trovato nessuna variabile
        if(nomeVariabile==null)
            throw new RuntimeException("Variabile non esistente");
        
        //Scorro tutta la lista dei powerSet,ed per ogni poliedro di ciascun powerSet vedo se c'e' almeno un poliedro che soddisfa il vincolo
        Iterator<PowerSet> iteratorPowerSet = listPowerSet.iterator();
        Iterator<Polyhedron> iteratorPolyhedrons;
        while(iteratorPowerSet.hasNext()){
            //Mi prendo il powerSet
            iteratorPolyhedrons=iteratorPowerSet.next().getListPolyhedrons().iterator();
         
            //Mi scorro i poliedri
            while(iteratorPolyhedrons.hasNext()){
                upperLowerBoundVariable =iteratorPolyhedrons.next().getUpperLowerBoundVariable(nomeVariabile);
                    if(upperLowerBoundVariable!=null){
                        if(value>=upperLowerBoundVariable.getFirst() && value<=upperLowerBoundVariable.getSecond())
                            ris=true;
                    }else
                        ris=true;
            }
        }
        return ris;
   }

    //Espressione regolare per individuare le componenti del PowerSet 
    private static final Pattern searchPowerSet =
	// was: Pattern.compile("(\\s*)([^\\(\\)\\{\\}]*)(\\s*)(\\()(\\s*)(\\{)(\\s*)(.*)(\\s*)(\\})(\\s*)");
	Pattern.compile("\\s*([^\\(\\)\\{\\}]*)\\s*(\\()?\\s*\\{\\s*(.*)\\s*\\}\\s*\\)?\\s*");
    private static final int POWERSET_NAME_GROUP = 1, POLYHEDRA_GROUP = 3;
    //Espressione regolare per individuare il PowerSet
    private static final Pattern delimiterPowerSet = Pattern.compile("\\n"); 
	// was: Pattern.compile("(\\))");
    //Espressione regolare per individuare i poliedri all'interno del PowerSet
    private static final Pattern searchPoly = Pattern.compile("(\\})(\\s*)(\\{)");

    
    public static void loadPoliedri(File filePoly,PPLModel modelPolyhedrons) throws FileNotFoundException{
        
        Scanner in=new Scanner(filePoly);
        
        //Matchaer per trovare i powerSet
        Matcher matcherPowerSet;
        //Scanner per scansionare i poliedri
        Scanner scannerPoly;
        
        //Setto lo scanner in modo che individua i vari powerSet
        in=in.useDelimiter(delimiterPowerSet);
        
        //Variabile che rappresenta l'insieme dei poliedri
        Pointset_Powerset_NNC_Polyhedron PowerSetNNCPolyhedron=null;
        //Variabile che rappresenta il singolo poliedro 
        NNC_Polyhedron NNCpoliedro;
        
        String tempPowerSet;
        
        //Continuo fin quando c'e' qualcosa da leggere nel file
        while(in.hasNext()) {
            //Prelevo il powerSet
            tempPowerSet = in.next();

	    // DEBUG
	    // System.out.println("**" + tempPowerSet + "**");

            //Per evitare il caso in cui ho una stringa vuota
            if(!tempPowerSet.matches("\\s*")) {
                //Primo controllo sulla sintassi prelevata
                matcherPowerSet = searchPowerSet.matcher(tempPowerSet);
            
                if (matcherPowerSet.matches()){
                    //Individuo i vari polyhedron all'interno del PowerSet

		    // DEBUG
		    // System.out.println("Reading " + matcherPowerSet.group(POWERSET_NAME_GROUP) + ": " + matcherPowerSet.group(POLYHEDRA_GROUP));

                    scannerPoly=new Scanner(matcherPowerSet.group(POLYHEDRA_GROUP));
                    scannerPoly.useDelimiter(searchPoly);
                    while(scannerPoly.hasNext()){
                        String sistemaVincoli=scannerPoly.next();
                        NNCpoliedro = creaPoliedroNNC(sistemaVincoli,modelPolyhedrons);
                        /*
                        * Se il Poliedro appena creato non usa tutte le variabili fino ad ora create le aggiungo aumentando lo spazio dimensionale
                        * ed uso il metodo add_space_dimensions_and_embed che non vincola queste variabili,ma possono assumere qualsiasi valore
                        * se ho il caso inverso che i Poliedri fino ad ora creati hanno uno spazio dimensinale inferiore al poliedro appena creato
                        * aumento lo spazio dimensionale di tutti i poliedri creati ed lo faccio tramite lo stesso metodo ma della classe Pointset_Powerset_NNC_Polyhedron
                        */
                        //Se non e' stato creato nessun poliedro 
                        if(NNCpoliedro==null)
                            throw new IllegalArgumentException("ErroreFile:Nessun poliedro caricato");

                        if(NNCpoliedro.space_dimension()<modelPolyhedrons.spaceDimension())
                            NNCpoliedro.add_space_dimensions_and_embed(modelPolyhedrons.spaceDimension()-NNCpoliedro.space_dimension());
               
			// DEBUG
			System.out.println("Read " + NNCpoliedro);
			
                        if(PowerSetNNCPolyhedron==null) //Vedo se e' il primo NNCpoliedro creato
                            PowerSetNNCPolyhedron=new Pointset_Powerset_NNC_Polyhedron(NNCpoliedro);
                        else{
                            if(NNCpoliedro.space_dimension()>PowerSetNNCPolyhedron.space_dimension())//Caso in cui i poliedri gia' creati hanno spazio dimensionale inferiore
                                PowerSetNNCPolyhedron.add_space_dimensions_and_embed(NNCpoliedro.space_dimension()-PowerSetNNCPolyhedron.space_dimension());
                        
                            PowerSetNNCPolyhedron.add_disjunct(NNCpoliedro);
                        }
                    }
                } else {
		    System.out.println("Syntax error");
                    throw new IllegalArgumentException("ErroreFile:Sintassi non corretta");
		}

                //Aggiungo il PowerSet alla lista se questo non e' vuoto
                if(PowerSetNNCPolyhedron!=null && !PowerSetNNCPolyhedron.is_empty())
                    modelPolyhedrons.addPowerSetNNCPoliedri(PowerSetNNCPolyhedron,
							    matcherPowerSet.group(POWERSET_NAME_GROUP));
                else
                    throw new IllegalArgumentException("ErroreFile:Nessun poliedro trovato");
            
                //Metto a null per il prossimo eventuale insieme di poliedri
                PowerSetNNCPolyhedron=null;
            }
        }
        //Se non ho caricato nessun poliedro lancio un eccezione
        if(modelPolyhedrons.isEmptyPolyhedrons())
             throw new IllegalArgumentException("ErroreFile:Nessun poliedro trovato");
    }
    
    // Espressione regolare che individua il singolo vincolo
    private static final Pattern vincolo = 
       Pattern.compile("([^&,<=>]+)\\s*(>=|>|<|<=|==)\\s*((\\+|-)?\\s*\\d+)");
    private static final int EXPR_GROUP = 1, REL_SYMBOL_GROUP = 2, CONST_TERM_GROUP = 3;
    // Espressione regolare che individua la singola variabile con relativo coefficiente all'interno del vincolo
    private static final Pattern variabili = 
	Pattern.compile("(\\+|-)?\\s*((\\d+)\\s*\\*\\s*)?(\\w+)");
    private static final int SIGN_GROUP = 1, COEFF_GROUP = 3, VAR_NAME_GROUP = 4;                                        

    private static NNC_Polyhedron creaPoliedroNNC(String vincoli, PPLModel modelPolyhedrons)
	throws IllegalArgumentException{
        Matcher trovaVincoli = vincolo.matcher(vincoli);
        Matcher trovaVariabili;
        
        // PPL objects
        Constraint_System sistemaVincoli = new Constraint_System(); 
        Constraint vincoloPPL;
	// The two default coefficients
        Coefficient one = new Coefficient(1), minusOne = new Coefficient(-1); 
        
        // Cerca i vincoli all'interno dell'insieme,ogni iterazione trova un vincolo
        while (trovaVincoli.find()) {

	    // DEBUG
	    // System.out.println("Analyzing " + trovaVincoli.group(0));

            // Uso un espressione a parte per trovare le varie variabili all'interno del vincolo trovato
            trovaVariabili = variabili.matcher(trovaVincoli.group(EXPR_GROUP));
            Linear_Expression expr = null;

            if (trovaVariabili==null)
                 throw new IllegalArgumentException("FileError: Syntax error in polyhedron file");
            
            //Cerca le varie variabili con i corrispettivi coefficienti all'interno del vincolo
            while (trovaVariabili.find()) {
                
                String variableName = trovaVariabili.group(VAR_NAME_GROUP);
          
               /* 
                * Leggenda gruppi:
                *   Il gruppo 6 rappresenta il segno della variabile senza coefficiente
                *   Il gruppo 3 rappresenta il coefficiente con segno della variabile
                * 
                * Ragionamento:
                *    Se il gruppo 6 e' null ed il gruppo 3 e' diverso da null 
		     vuol dire che la variabile ha un coefficiente (diverso da 1) con segno;
                *    altrimenti se il gruppo 6 e' null  oppure e' uguale al segno '+',
		     uso il coefficiente positivo per la variabile altrimenti vuol
                *    dire che la varibile ha segno '-' (perche' se il gruppo 6 non e' nullo ed non e' '+' allora sara uguale ad '-')
                */

		Coefficient coefficient;
		String sign = trovaVariabili.group(SIGN_GROUP);
		
                if (trovaVariabili.group(COEFF_GROUP)!=null)
                    coefficient = new Coefficient( ((sign==null)?"":sign) + trovaVariabili.group(COEFF_GROUP));
                else {    
                    if (sign==null || sign.equals("+"))
                        coefficient = one;
                    else
                        coefficient = minusOne;
                }

		// DEBUG
		// System.out.println("Coefficient, varname: " + coefficient + ", " + variableName);

                // Se temp e' null creo un espressione con la sola variabile,
		// altrimenti sommo la variabile all'espressione in costruzione
		
		Linear_Expression term = new Linear_Expression_Times(coefficient, 								                                                  modelPolyhedrons.createVariable(variableName));
                if (expr==null)     
                    expr = term;
                else
                    expr = expr.sum(term);
            } // end internal while

            if(expr==null)
                throw new IllegalArgumentException("FileError: Syntax error in LHS of a constraint");

	    // Creo il vincolo PPL ed lo aggiungo al sistema in costruzione
            vincoloPPL = new Constraint(expr, 
					modelPolyhedrons.relationSymbols.get(trovaVincoli.group(REL_SYMBOL_GROUP)),
					new Linear_Expression_Coefficient(
					    new Coefficient(trovaVincoli.group(CONST_TERM_GROUP))));
            sistemaVincoli.add(vincoloPPL);
        } // end while
	
        return new NNC_Polyhedron(sistemaVincoli);
    }
    
    public static void savePowerSets(String variableDraw1,String variableDraw2,String nomePowerSet,Pointset_Powerset_NNC_Polyhedron powerSet,BufferedWriter writer) throws IOException{
        //Setto la corrispondenza delle variabili per la stampa
        setStringVariableDraw(variableDraw1,variableDraw2);
        //Indice per ciclare sul powerSet
        long indexPoly;
        //Variabile temporanea dei polyhedron
        NNC_Polyhedron polyhedrons;
        //Iteratore sul powerSet
        Pointset_Powerset_NNC_Polyhedron_Iterator begin_iterator = powerSet.begin_iterator();
        //Parto da zero
        indexPoly=0;
        //Stringa temporanea
        String polyString;
        //Scrivo il nome del PowerSet sul file
        writer.write(nomePowerSet+"(");
        //Scorro i vari poliedri del powerSet per salvarli sul file
        while(indexPoly<powerSet.size()){
            polyhedrons = begin_iterator.get_disjunct();
             //Preparo la string del powerSet se il poliedro non e' vuoto
            if(!polyhedrons.is_empty()){
                polyString="{"+polyhedrons.toString().replace(",", " &")+"}";
                //Scrivo sul file il poliedro
                writer.write(polyString);
            }
            
            //Avanzo sia con l'iteratore che con l'indice
            begin_iterator.next();
            indexPoly++;
        }
        //Chiudo l'insieme del powerSet
        writer.write(") ");
    }
    //Metodo che gestisce la corrispondenza delle variabili di PPL ed quella dell'utente
    private static void setStringVariableDraw(final  String variableDraw1,final String variableDraw2 ){
        Variable.setStringifier(new Variable_Stringifier(){
            @Override  
            public String stringify (long var_id){
                if(variableDraw1==null || variableDraw2==null || variableDraw1.isEmpty() || variableDraw2.isEmpty())
                    throw new RuntimeException("Errore:Nessuna variabile settata");
                if(var_id==0)
                    return variableDraw1;
                else
                    return variableDraw2;
            }
        });
    }   
}

