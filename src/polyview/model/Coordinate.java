
package polyview.model;
import java.util.Comparator;
import parma_polyhedra_library.Generator_Type;
/**
 *
 * @author nello
 */
public class Coordinate implements Comparable<Coordinate>{
    private VariableValue variableDrawAxis1;
    private VariableValue variableDrawAxis2;
    Generator_Type type;
    
    public static final Comparator<Coordinate> comparatorYCoordinate=new Comparator<Coordinate>(){
        @Override
        public int compare(Coordinate x,Coordinate y){
            int compare=0;
            if(x.getCoordinateAxis2()==y.getCoordinateAxis2()){
                if(x.getCoordinateAxis1()-y.getCoordinateAxis1()>0)
                    compare=1;
                else if(x.getCoordinateAxis1()-y.getCoordinateAxis1()<0)
                    compare=-1;
            }
            else if(x.getCoordinateAxis2()>y.getCoordinateAxis2())
                 compare=1;
            else
                compare=-1;
        
        return compare;
        }
        
    };
     
    
    public Coordinate (VariableValue variableDraw1,VariableValue variableDraw2,Generator_Type type) {
        if(variableDraw1==null || variableDraw2==null || type==null)
            throw new RuntimeException("Riferimenti di coordinate nulli");
        this.variableDrawAxis1=variableDraw1;
        this.variableDrawAxis2=variableDraw2;
        this.type=type;
    }
        
    public String getVariableDrawAxis1() {
        return variableDrawAxis1.getVariable();
    }
    public String getVariableDrawAxis2() {
        return variableDrawAxis2.getVariable();
    }
    public double getCoordinateAxis1() {
        return variableDrawAxis1.getValue();
    }
    
    public double getCoordinateAxis2() {
        return variableDrawAxis2.getValue();
    }
    
    public CoordinateType getTypeCoordinate(){
        CoordinateType temp=null;
        int ordinalType=type.ordinal();
        
        switch(ordinalType){
            case 0: temp=CoordinateType.LINE;
                    break;
            case 1: temp=CoordinateType.RAY;
                    break;
            case 2: temp=CoordinateType.POINT;
                    break;
            case 3: temp=CoordinateType.CLOSURE_POINT;
                    break; 
        }
        return temp;
            
    }
    
    @Override
    public boolean equals(Object temp){
        if(temp==null)
            return false;
        //Controllo se l'oggetto è un sottotipo della classe Z
        if(!(temp instanceof Coordinate))
            return false;
        
        //Posso fare il cast senza avere errori perchè sarà s
        Coordinate x=(Coordinate)temp;
        //Se hanno entrambi i valori uguali delle due variabili allora sono uguali
        if(this.getCoordinateAxis1()==x.getCoordinateAxis1() && this.getCoordinateAxis2()==x.getCoordinateAxis2())
            return true;
        else
            return false;
    }
    
    @Override
    public int compareTo(Coordinate o) {
        int compare=0;
        if(this.getCoordinateAxis1()==o.getCoordinateAxis1())
            if(this.getCoordinateAxis2()-o.getCoordinateAxis2()>0)
                compare=1;
            else
                compare=-1;
        else if(this.getCoordinateAxis1()-o.getCoordinateAxis1()>0)
                 compare=1;
        else
            compare=-1;
        
        return compare;
    }
    
    public enum CoordinateType{
       LINE,RAY,POINT,CLOSURE_POINT;
    }
}
