
package polyview.model;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Collections;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;
import parma_polyhedra_library.Coefficient;
import parma_polyhedra_library.Constraint;
import parma_polyhedra_library.Constraint_System;
import parma_polyhedra_library.Generator;
import parma_polyhedra_library.Linear_Expression;
import parma_polyhedra_library.Linear_Expression_Coefficient;
import parma_polyhedra_library.Linear_Expression_Times;
import parma_polyhedra_library.NNC_Polyhedron;
import parma_polyhedra_library.Pointset_Powerset_NNC_Polyhedron;
import parma_polyhedra_library.Pointset_Powerset_NNC_Polyhedron_Iterator;
import parma_polyhedra_library.Relation_Symbol;
import parma_polyhedra_library.Variable;
import parma_polyhedra_library.Variables_Set;

/**
 *
 * @author nello
 */
public class PowerSet {
    //Riferimento alla classe PPL dei powerSetNNC_Polyhedron
    private final Pointset_Powerset_NNC_Polyhedron powerSetNNCPoly;
    //Lista di poliedri nella rappresentazione Poliedri
    private final List<Polyhedron> listPolyhedronNNC;
    //Nome associato al powerSet
    private final String nomePowerSet;
    //Colore associato al powerSet
    private Color color;
    //Riferimento al modello di appartenenza
    private PPLModel modello;
    
    public PowerSet(Pointset_Powerset_NNC_Polyhedron powerSetNNCPoly,String nomePowerSet,Color color,PPLModel modello){
        //Controllo che i parametri ricevuti non siano nulli
        if(powerSetNNCPoly==null||nomePowerSet==null||color==null||modello==null)
            throw new RuntimeException("Errore costruttore PowerSet:parametri nulli");
        //Inizializzo i vari campi
        this.powerSetNNCPoly=powerSetNNCPoly;
        this.nomePowerSet=nomePowerSet;
        this.color=color;
        this.modello=modello;
        
        //Inizializzo la lista di poliedri che rappresenta il powerSet
        listPolyhedronNNC=new ArrayList<>();
        //Mi costruisco i poliedri contenuti nel powerSet
        loadPoliedri();
    }
    
    private void loadPoliedri(){
        
        //Indice utilizzato nel while che scorre i poliedri dell'iteratore dei vari poliedri 
        long indexPoly;
        //Iteratore Poliedri
        Pointset_Powerset_NNC_Polyhedron_Iterator iteratoreNNCpoliedri;
        //Variabili rappresentati il poliedro nelle due forme
        NNC_Polyhedron polyConstraint;
        Polyhedron temp;

        //Scorro i poliedri del PowerSet
        indexPoly=0;
        iteratoreNNCpoliedri=powerSetNNCPoly.begin_iterator();
        
        while(indexPoly<powerSetNNCPoly.size()){
            //Mi procuro il poliedroNNC
            polyConstraint=iteratoreNNCpoliedri.get_disjunct();
            //Creo l'oggetto rappresentate il NNC_polyhedrons
            temp=new Polyhedron(polyConstraint,modello.newBoxValue(polyConstraint));
            //Aggiorno il newBoxValue
            if(temp==null)
                throw new RuntimeException("DrawCoordinatesPoly null");
            //Passo al prossimo NNCPolyhedron
            iteratoreNNCpoliedri.next();
            //Incremento l'indexPoly per un confronto successivo con la grandezza della lista(questo perchè questo iteratore non ha hasNext)
            indexPoly++;
           //Aggiungo la lista di coordinate del NNCPolyhedron caricato 
            listPolyhedronNNC.add(temp);
        } 
    }
    
    public List<List<Coordinate>> setPowerSet(PPLModel.ModelFrame variableFrame){
        List<List<Coordinate>> listDrawPoint=new ArrayList<>();
        List<Coordinate> temp;
        //Scorro tutti i poliedri appartenenti al powerSet ed setto i vari polyhedron in base alle scelte dell'utente
        for(Polyhedron poly:listPolyhedronNNC){
            temp=poly.setPoly(variableFrame);
            //Se ho un poliedro nullo(cioè con i settaggi dell'utente è vuoto) non lo inserisco
            if(temp!=null)
                listDrawPoint.add(temp);
        }
        return listDrawPoint;
    }
    
    public PowerSetView createPowerSetFrame(){
        
        return new PowerSetView();
    }
    
    public class PowerSetView{
        private List<List<Coordinate>> listPolyhedrons;
        
        public List<List<Coordinate>> getListPolyhedrons(){
            return listPolyhedrons;
        }
        public Color getColorPowerSet(){
            return PowerSet.this.getColorPowerSet();
        }
        protected void setListPowerSet(List<List<Coordinate>> listPowerSet){
             this.listPolyhedrons=listPowerSet;
        }
    }
    
    public List<Polyhedron> getListPolyhedrons(){
        return listPolyhedronNNC;
    }
    public Pointset_Powerset_NNC_Polyhedron getPointsetPowerSet(){
        return powerSetNNCPoly;
    }
    public String getNomePowerSet(){
        return nomePowerSet;
    }
    public Color getColorPowerSet(){
        return color;
    }
    public void setColorPowerSet(Color color){
        this.color=color;
    }

    public class Polyhedron {
    
        private NNC_Polyhedron poliedro;
        private Coordinate lowestPoint;
        private Map<Long,Pair<Float>> rangeVariablePoly;
    
    
        private final Comparator<Coordinate> compareAreaCoordinate=new Comparator<Coordinate>(){
        
            @Override
            public int compare(Coordinate x,Coordinate y){
                if(lowestPoint==null)
                    throw new RuntimeException("Lowest point is null");
            
                double area=area2(lowestPoint,x,y);
                double px,py;
                int compare=0; //Se ritorna compare=0 significa che siamo rientrati nel caso in cui i punti sono coincidenti
                if(area>0)
                    compare=-1;
                else if(area<0)
                    compare=1;
                else{ //Sono collineari con lowestPoint
                    px=Math.abs(x.getCoordinateAxis1()-lowestPoint.getCoordinateAxis1())-Math.abs(y.getCoordinateAxis1()-lowestPoint.getCoordinateAxis1());
                    py=Math.abs(x.getCoordinateAxis2()-lowestPoint.getCoordinateAxis2())-Math.abs(y.getCoordinateAxis2()-lowestPoint.getCoordinateAxis2());
                
                    if(px<0 || py<0)
                        compare=-1;
                    else if(px>0 || py>0)
                        compare=1;
                }
            
                return compare;
            }
            
            private double area2(Coordinate origine,Coordinate p1,Coordinate p2){
                return (p1.getCoordinateAxis1()-origine.getCoordinateAxis1()) * (p2.getCoordinateAxis2()-origine.getCoordinateAxis2()) - (p1.getCoordinateAxis2()-origine.getCoordinateAxis2()) * (p2.getCoordinateAxis1()-origine.getCoordinateAxis1());
            }
        };
    
        private void setLowestPoint(Coordinate lowestPoint){
            this.lowestPoint=lowestPoint;
        }
    
        public Polyhedron(NNC_Polyhedron poliedro,Map<Long,Pair<Float>> rangeVariablePoly){
            if(poliedro==null || modello==null || rangeVariablePoly==null)
                throw new RuntimeException();
        
                this.poliedro=poliedro; 
                this.rangeVariablePoly=rangeVariablePoly;
        }
    
        public boolean isBounded(){
            return poliedro.is_bounded();
        }
        protected NNC_Polyhedron getNNCPoliedro(){
            return this.poliedro;
        }
    
        public List<Coordinate> setPoly(PPLModel.ModelFrame modelFrame){
        
            //Variabili per la creazione
	    double divisor;
            int index, variableDraw1, variableDraw2;
            int indexCoordinate;
            NNC_Polyhedron polyTemp;
        
            //Lista da restituire
            List<Coordinate> listDrawPoint=null;
        
            //Rappresentano la nuova coordinata,la coordinata più bassa ed estrema dx
            Coordinate newCoordinate,lowestCoordinate=null;
        
            //Scanner della stringa Ascii
            Scanner in;
            //L'inizio della struttura della stringa ascii inizia con 0
            Pattern start=Pattern.compile("size\\s.");
            //Variabili temporanee per salvare la coppia<Variable,valore variable>
            VariableValue variableValue1=null;
            VariableValue variableValue2=null;
            //Creo un nuovo poliedro con i valori delle variabili sezionate(non modifico l'originale)
            polyTemp=addVariableValue(modelFrame);
            //Se con i settaggi dell'utente ottengo un poliedro non vuoto proseguo con le operazioni
            if(polyTemp!=null){
                //Inizializzo la lista che dovrà contenere i vertici da disegnare
                listDrawPoint=new ArrayList<>();
                //Controllo che sia a due dimensioni
                if(polyTemp.space_dimension()!=2)
                    throw new RuntimeException("Errore intersezione:il poliedro generato dai vincoli non è di due dimensioni");
            
                // Interseco il poliedro ottenuto dopo il sezionamento 
		// con il rettangolo della ViewPort, ottenendo un poliedro sempre chiuso
                polyTemp.intersection_assign(createViewPort());
            
                //Scorro i generatori del poliedro
                for(Generator gen: polyTemp.generators()){
                    divisor = 1.0;
                    //Inizializzo lo scanner
		    String gen_string = gen.ascii_dump();

		    // System.out.println("*" + gen_string +  "*"); // DEBUG

                    in = new Scanner(gen_string);
                    //Mi porto al coefficiente della prima Variable
                    in.findInLine(start);
                
                    if(in.hasNext()){
                        divisor = in.nextBigInteger().doubleValue();
                        if (divisor == 0)
                            divisor = 1;
                    }
                
                    /* Variabili temporanee che rappresentano gli indici delle variabili 
		       da disegnare (gli indici sono 1 e 0 perchè ho rimosso le dimensioni superiori a 2)
		       e di conseguenza gli indici delle variabili originarie sono stati scalati,
		       ed il maggiore avrà valore 1 ed il minore il valore 0.
		    */
                    if(modelFrame.getVariablesDraw().getFirst()>modelFrame.getVariablesDraw().getSecond()){
                        variableDraw1 =1;
                        variableDraw2 =0;
                    }else{
                        variableDraw1 =0;
                        variableDraw2 =1;
                    }
                
                    //Indice per stabilire il valore corrispondente alla variabile
                    index=0;
                    //Prosegue finchè ci sono valori ed gli indici delle variabili che mi interessano ancora non le ho perlustrate             
                    while(in.hasNextBigInteger()&&(variableDraw1>=index || variableDraw2>=index)){
                        //Se l'indice è uguale all'indice della prima variabile creo la coppia Variable/Value 
                        //e cosi per la seconda variabile oppure continuo a perlustrare
			double value = in.nextBigInteger().doubleValue() / divisor;

                        if(index==variableDraw1)
                            variableValue1 = 
				new VariableValue(modello.getStringVariable(modelFrame.getVariablesDraw().getFirst()), value);
                        else if(index==variableDraw2)
                            variableValue2=
				new VariableValue(modello.getStringVariable(modelFrame.getVariablesDraw().getSecond()), value);
                        index++;
                    }
            
                    //Se gli indici delle variabili sono maggiori dell'indice di perlustrazione vuol dire che hanno valore zero
                    if(variableDraw1>=index)
                        variableValue1=new VariableValue(modello.getStringVariable(modelFrame.getVariablesDraw().getFirst()),0);
                    if(variableDraw2>=index)
                        variableValue2=new VariableValue(modello.getStringVariable(modelFrame.getVariablesDraw().getSecond()),0);
            
                    newCoordinate=new Coordinate(variableValue1, variableValue2, gen.type());
                
                    //Aggiungo le coordinate del generator(il quale per via dell'intersezione è sicuramente un Point)
                    listDrawPoint.add(newCoordinate);
                    //Inizializzo la coordinata lowestCoordinate
                    if((lowestCoordinate==null)||(newCoordinate.getCoordinateAxis2()<lowestCoordinate.getCoordinateAxis2()) ||(newCoordinate.getCoordinateAxis2()==lowestCoordinate.getCoordinateAxis2() && newCoordinate.getCoordinateAxis1()>lowestCoordinate.getCoordinateAxis1()))
                        lowestCoordinate=newCoordinate;
                    }

                    //Swap tra lowest e first Coordinate 
                    indexCoordinate=listDrawPoint.indexOf(lowestCoordinate);
                    newCoordinate=listDrawPoint.set(0, lowestCoordinate);
                    listDrawPoint.set(indexCoordinate,newCoordinate);

                    //Imposto il lowestPoint per l'ordinamento
                    setLowestPoint(lowestCoordinate);
                    Collections.sort(listDrawPoint, compareAreaCoordinate); //Ordino i punti in base al compare dell'area
  
                    //Ordino le coordinate in senso antiorario per costruire l'insieme convesso
                    listDrawPoint=convexHull(listDrawPoint);
            }
        
            //Ritorno la lista costruita
            return listDrawPoint;
        }
    
        //Applicato l'algortimo Graham's Algorithm
        public List<Coordinate> convexHull(List<Coordinate> drawCoordinatestPoly){
            //Dimensione della lista delle coordinate
            int size=drawCoordinatestPoly.size();
            //Indice degli elementi dello stack
            int top;
            //Indice degli elementi della lista
            int i;
            
            //Lista del convexHull
            List<Coordinate> convexHull;
            
            //Se ho più di due elementi ha senso applicare la procedura
            if(size>2){
                //Inizializzare lo stack con i primi due elementi(Sicuro vertexExtreme)
                convexHull=new ArrayList<>();
                convexHull.add(drawCoordinatestPoly.get(0));
                convexHull.add(drawCoordinatestPoly.get(1));
                top=2;
                i=2;
                
                while(i < size){
                    //Se ho una svolta a sx aggiungo il vertice nello stack
                    if(isLeft(convexHull.get(top - 2),convexHull.get(top - 1),drawCoordinatestPoly.get(i))>=0){
                        convexHull.add(top,drawCoordinatestPoly.get(i));//lo aggiungo
                        top++;
                        i++;
                    }else{ //Se ho una svolta a dx formo un dente(non convesso) ed lo rimuovo dallo stack
                        convexHull.remove(top-1);
                        top--;
                    }
                }
            }
            else
                convexHull=drawCoordinatestPoly; //Non ho bisogno dell'ordinamento
   
            return convexHull;
        }
        
        /**orientamento(A,B,C) ritorna:
        * valore maggiore di 0 se C si trova a sx della linea tra A e B
        * valore minore di 0 se C si trova a dx della linea tra A e B
        * valore uguale di 0 se C si trova sulla linea tra A e B.
        */
        public double isLeft(Coordinate origine,Coordinate p1,Coordinate p2){
            return (p1.getCoordinateAxis1()-origine.getCoordinateAxis1()) * (p2.getCoordinateAxis2()-origine.getCoordinateAxis2()) - (p1.getCoordinateAxis2()-origine.getCoordinateAxis2()) * (p2.getCoordinateAxis1()-origine.getCoordinateAxis1());
        }
    
        public NNC_Polyhedron addVariableValue(PPLModel.ModelFrame variableFrame){
     
            //Espressioni lineari della parte sx e dx
            Linear_Expression expSx;
            Linear_Expression expDx;
            //Inizializzo il sistema di constraint
            Constraint_System vincolo;
            vincolo=new Constraint_System();
            //Insieme che costruisco iterativamente per tenere traccia della variabili sezionate
            Variables_Set setVariable=new Variables_Set();
            //Scorro le variabili sezionate ed creo il vincolo di sezionamento aggiungendolo all'insieme delle variabili e dei vincoli
            for(Long temp:variableFrame.getVariablesValue().keySet()){
                
                //Creo ed aggiungo il vincolo al sistema per sezionare la variabile corrente
                expSx=new Linear_Expression_Times(new Coefficient(1l),modello.createVariable(modello.getStringVariable(temp)));
                expDx=new Linear_Expression_Coefficient(new Coefficient(variableFrame.getVariablesValue().get(temp)));
                vincolo.add(new Constraint(expSx,Relation_Symbol.EQUAL,expDx));
                //Mi salvo la variabile sezionata nell'insieme
                setVariable.add(new Variable(temp));
            }
        
            //Mi creo una copia del poliedro originale
            NNC_Polyhedron poly1=new NNC_Polyhedron(poliedro);
            //Vedo se sto nel caso in cui lo spazio dimensionale del poliedro appartenente ad un PowerSet preso in considerazione sia minore dello spazio dimensionare di qualche altro powerSet 
            if(poly1.space_dimension()<modello.spaceDimension())
                poly1.add_space_dimensions_and_embed(modello.spaceDimension()-poly1.space_dimension());//Aggiungo le dimensioni mancanti
           
            //Aggiungo i nuovi vincoli alla copia del poliedro originale
            poly1.add_constraints(vincolo);
            if(!poly1.is_empty()){
                //Aggiungo le restanti variabili non sezionate all'insieme (cioè le variabili proiettate)(controllare meglio anche questa operazione)
                for(Long temp:variableFrame.getVariablesProject())
                    setVariable.add(new Variable(temp));
            
                //Rimuovo le variabili che non sono state destinate al draw
                poly1.remove_space_dimensions(setVariable);
            }
            else //Se il constraint non va bene metto il poliedro a null
            poly1=null;
            //Ritorno il poliedro sezionato oppure null nel caso in cui il sezionamento ha generato un poliedro nullo
            return poly1;
        }
    
        public NNC_Polyhedron createViewPort(){
        
            Linear_Expression espressioneDx;
            //Mi creo il coefficiente 1 che mi serve per le variabili
            Coefficient c1=new Coefficient(1);
            //Mi creo i coefficienti che mi servono per definire la viewPort
            Coefficient cMinFirst=new Coefficient(-1000);
            Coefficient cMaxFirst=new Coefficient(1000);
            Coefficient cMinSecond=new Coefficient(-1000);
            Coefficient cMaxSecond=new Coefficient(1000);
            //Sistema di constraint per la ViewPort
            Constraint_System sistemaVincoli=new Constraint_System();
        
            //Coefficienti con le rispettive variabili(uso semplicemente la Variable 0 ed 1 visto che quando rimuovo lo spazio dimensionale mi porta sempre a questa situazione)
            Linear_Expression c1FirstVariable=new Linear_Expression_Times(c1,new Variable(0));
            Linear_Expression c1SecondVariable=new Linear_Expression_Times(c1,new Variable(1));
        
            espressioneDx=new Linear_Expression_Coefficient(cMinFirst);
            Constraint vincolo=new Constraint(c1FirstVariable,Relation_Symbol.GREATER_OR_EQUAL,espressioneDx); 
            sistemaVincoli.add(vincolo);
        
            espressioneDx=new Linear_Expression_Coefficient(cMaxFirst);
            vincolo=new Constraint(c1FirstVariable,Relation_Symbol.LESS_OR_EQUAL,espressioneDx);
            sistemaVincoli.add(vincolo);
        
            espressioneDx=new Linear_Expression_Coefficient(cMinSecond);
            vincolo=new Constraint(c1SecondVariable,Relation_Symbol.GREATER_OR_EQUAL,espressioneDx);
            sistemaVincoli.add(vincolo);
        
            espressioneDx=new Linear_Expression_Coefficient(cMaxSecond);
            vincolo=new Constraint(c1SecondVariable,Relation_Symbol.LESS_OR_EQUAL,espressioneDx);
            sistemaVincoli.add(vincolo);
        
            return  new NNC_Polyhedron(sistemaVincoli);
        }

        public Pair<Float> getUpperLowerBoundVariable(String variable){
            return rangeVariablePoly.get(modello.getIndiceVariable(variable));
        }
    }
}
