package polyview;
import polyview.view.MainFrame;

/**
 *
 * @author nello
 */
public class Main {
    
    /**
     * @param args the command line arguments
     */
    public static void main(final String args[]) {
        
        /* Imposto Nimbus come look and feel */

        /* Se Nimbus (introduced in Java SE 6) non e' disponibile,rimane con il look and feel di default.
         * Per dettagli consultare http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        
        try{
            javax.swing.UIManager.LookAndFeelInfo[] installedLookAndFeels=javax.swing.UIManager.getInstalledLookAndFeels();
                for (int idx=0; idx<installedLookAndFeels.length; idx++)
                    if ("Nimbus".equals(installedLookAndFeels[idx].getName())) {
                        javax.swing.UIManager.setLookAndFeel(installedLookAndFeels[idx].getClassName());
                        break;
                    }
        }
        catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } 
        catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } 
        catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

	// Requires java.library.path to be correctly set
	System.loadLibrary("ppl_java"); 
	// otherwise, specify a full path: 
	//   System.load("/Users/mfaella/local/lib/ppl/libppl_java.jnilib"); 
     
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainFrame(args).setVisible(true);
            }
        });
    }
}
