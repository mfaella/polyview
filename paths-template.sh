#!/bin/bash

# full path of jogl-all.jar
JOGL_JAR="/home/spex/jogl/jar/jogl-all.jar"

# full path of gluegen[2]-rt.jar
GLUEGEN_JAR="/home/spex/jogl/jar/gluegen-rt.jar"

# full path to libgluegen-rt.so or libgluegen2-rt.so, no slash at the end
GLUEGEN_PATH="/home/spex/jogl/lib"

# full path to libppl_java.{so,jnilib}, no slash at the end
PPL_PATH="/home/spex/ppl-1.2/interfaces/Java/jni/.libs"

# full path of ppl_java.jar
PPL_JAR="/home/spex/ppl-1.2/interfaces/Java/ppl_java.jar"
